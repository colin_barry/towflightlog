package com.paragontek.barry.TowFlightLog;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.paragontek.barry.TowFlightLog.R;

import java.io.File;
import java.util.ArrayList;

public class TowLogHistoryListView extends AppCompatActivity {

    File[] files = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow_log_history_list_view);

        TowPlaneApplication app = ((TowPlaneApplication) this.getApplication());
        files = app.getTowLogData().getTowLogHistory(TowLogHistoryListView.this);

        ArrayList<String> items = new ArrayList<String>();
        for (File file : files) {
            items.add(file.getName());
        }

        Resources res = getResources();
        ListView list = (ListView) findViewById(R.id.list);
        // https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);

        list.setAdapter(itemsAdapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(TowLogHistoryListView.this, TowLogListView.class);
                intent.putExtra("LogDataAbsolutePath", files[position].getAbsolutePath());
                TowLogHistoryListView.this.startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_return_to_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_main) {
            Intent mainIntent = new Intent(this, MainActivity.class);
            TowLogHistoryListView.this.startActivity(mainIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
