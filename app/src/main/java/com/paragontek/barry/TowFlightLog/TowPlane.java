package com.paragontek.barry.TowFlightLog;

import java.io.Serializable;

/**
 *
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 *
 *
 * Class to hold Tow Plane Objects
 */
public class TowPlane implements Serializable, Comparable<TowPlane> {

    private int id;
    private String code;
    private String description;
    private String imageUri;
    private boolean isActive;

    public TowPlane(int id, String code){
        this.id = id;
        this.code = code;
    }

    public TowPlane(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean active) {
        this.isActive = active;
    }

    /** Intended for debugging only.  */
    @Override public String toString() {
        return String.format("Id: %d Code: %s Description: %s Is Active: %b ImageUri: %s", getId(), getCode(), getDescription(), getIsActive(), getImageUri());
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public int compareTo(TowPlane other) {

        return code.compareTo(other.code);
    }
}
