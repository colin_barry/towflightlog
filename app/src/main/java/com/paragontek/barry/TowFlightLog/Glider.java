package com.paragontek.barry.TowFlightLog;

import java.io.Serializable;

/**
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 */
public class Glider implements Serializable, Comparable<Glider> {
    private int id;
    private String code;
    private String description;
    private Boolean isTwin;
    private Boolean isActive;
    private String imageUri;

    public Glider(){}

    public Glider(int id, String code, String description){
        this.id = id;
        this.code = code;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsTwin() {
        return isTwin;
    }

    public void setIsTwin(Boolean isTwin) {
        this.isTwin = isTwin;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    @Override
    public String toString() {
        return "Glider{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", isTwin=" + isTwin +
                ", isActive=" + isActive +
                ", imageUri='" + imageUri + '\'' +
                '}';
    }

    public int compareTo(Glider other) {

        return code.compareTo(other.code);
    }
}
