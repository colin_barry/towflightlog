package com.paragontek.barry.TowFlightLog;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Brendan on 4/27/15.
 */

//Tutorial used: http://androidexample.com/How_TO_CREATE_A_CUSTOME_LISTVIEW/index.php?view=article_discription&aid=67&aaid=92

public class TowPlaneListViewAdapter extends BaseAdapter{ 
    private ArrayList data;
    private static LayoutInflater inflater = null;
    TowPlane towPlaneValue = null;
    private Activity activity;
    public Resources res;

    public TowPlaneListViewAdapter(Activity a, ArrayList d, Resources resLocal){

        activity = a;
        data = d;
        res = resLocal;

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (data.size() <=0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public static class ViewHolder{

        public TextView code;
        public TextView description;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View vi = view;
        ViewHolder holder;

        if(vi == null){
            vi = inflater.inflate(R.layout.towplane_listview_format, null);

            holder = new ViewHolder();
            holder.code = (TextView) vi.findViewById(R.id.code);
            holder.description = (TextView) vi.findViewById(R.id.description);

            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        if(data.size()<=0)
        {
            holder.code.setText("No Data");
        }
        else
        {
            //Get each Model object from ArrayList
            towPlaneValue = null;
            towPlaneValue = ( TowPlane ) data.get( i );

            //Set Model values in Holder elements
            holder.code.setText(towPlaneValue.getCode());
            holder.description.setText(towPlaneValue.getDescription());
        }
        return vi;
    }
}
