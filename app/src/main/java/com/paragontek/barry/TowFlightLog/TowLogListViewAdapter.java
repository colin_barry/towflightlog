package com.paragontek.barry.TowFlightLog;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Brendan on 4/27/15.
 */

//Tutorial used: http://androidexample.com/How_TO_CREATE_A_CUSTOME_LISTVIEW/index.php?view=article_discription&aid=67&aaid=92

public class TowLogListViewAdapter extends BaseAdapter{ 
    private ArrayList data;
    private static LayoutInflater inflater = null;
    TowLog towLogValue = null;
    private Activity activity;
    public Resources res;
    PilotData pilotData = null;
    GliderData gliderData = null;

    public TowLogListViewAdapter(Activity a, ArrayList d, Resources resLocal){

        activity = a;
        data = d;
        res = resLocal;

        TowPlaneApplication app = ((TowPlaneApplication) a.getApplication());
        pilotData = TowPlaneApplication.getPilotData();
        gliderData = TowPlaneApplication.getGliderData();

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (data.size() <=0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public static class ViewHolder{

        public TextView pilotCode;
        public TextView pilotName;
        public TextView gliderCode;
        public TextView gliderName;
        public TextView height;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View vi = view;
        ViewHolder holder;

        if(vi == null){
            vi = inflater.inflate(R.layout.towlog_listview_format, null);

            //Get reference to text view fields and assign to view holder
            holder = new ViewHolder();
            holder.pilotCode = (TextView) vi.findViewById(R.id.pilotCode);
            holder.pilotName = (TextView) vi.findViewById(R.id.pilotName);
            holder.gliderCode = (TextView) vi.findViewById(R.id.gliderCode);
            holder.gliderName = (TextView) vi.findViewById(R.id.gliderName);
            holder.height = (TextView) vi.findViewById(R.id.height);

            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        if(data.size()<=0)
        {
            holder.pilotCode.setText("No Data");
        }
        else
        {
            //Get each Model object from ArrayList
            towLogValue=null;
            towLogValue= ( TowLog ) data.get( i );

            //Set Model values in Holder elements
            int pilotID = towLogValue.getPilotId();
            Pilot pilot = pilotData.findInstanceByID(pilotID);
            String pilotName = (pilot != null) ? pilot.getName() : "???";
            String pilotCode = (pilot != null) ? pilot.getCode() : "???";
            holder.pilotCode.setText(pilotCode);
            holder.pilotName.setText(pilotName);

            int gliderID = towLogValue.getGliderId();
            Glider glider = gliderData.findInstanceByID(gliderID);
            String gliderCode = (glider != null) ? glider.getCode() : "???";
            String gliderName = (glider != null) ? glider.getDescription() : "???";
            holder.gliderCode.setText(gliderCode);
            holder.gliderName.setText(gliderName);

            String height = String.valueOf(towLogValue.getReleaseAltitude());
            holder.height.setText(height);
         }
        return vi;
    }


}
