package com.paragontek.barry.TowFlightLog;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Brendan on 5/1/15.
 */
public class TowLogData implements Serializable {

    private static final String uploadURL = "http://ssb.michirado.com/flightlog/__upload_towlog_data";

    private ArrayList<TowLog> mtowLogs = new ArrayList<TowLog>();

    private static final String serializationName = "TowLogData";
    private static final String serializationCurrentName = "CurrentTowLog";

    public TowLogData(){}

    public static String getUploadURL() {
        return uploadURL;
    }

//    public TowLogData(ArrayList<TowLog> towLogs){
//        this.towLogs = towLogs;
//    }

    public ArrayList<TowLog> getTowLogs() {
        return mtowLogs;
    }

//    public void setTowLogs(ArrayList<TowLog> towLogs) {
//        this.towLogs = towLogs;
//    }

    public boolean isEmpty(){
        return mtowLogs == null || mtowLogs.size() == 0;
    }

    public void add(TowLog towLog){
        mtowLogs.add(towLog);
    }

    /**
     * Serialize a single tow log to "disk"
     * @param ctx
     * @param towLog       The tow log
     */
    protected void serializeCurrentTowLog(Context ctx, TowLog towLog) {
        File f = ctx.getFilesDir();
        String absolutePath = f.getAbsoluteFile() + File.separator + serializationCurrentName;
        final File suspend_f = new File(absolutePath);

        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(suspend_f);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(towLog);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (oos != null) oos.close();
                if (fos != null) fos.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Deserialize the current tow log entry
     * @param ctx
     */
    public TowLog deserializeCurrentTowLog(Context ctx) {
        File f = ctx.getFilesDir();
        String absolutePath = f.getAbsoluteFile() + File.separator + serializationCurrentName;
        final File suspend_f = new File(absolutePath);

        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(suspend_f);
            ois = new ObjectInputStream(fis);
            return (TowLog) ois.readObject();
        } catch (Exception e) {
            return null;     // Hack to initialize on error
        } finally {
            try {
                if (ois != null) ois.close();
                if (fis != null) fis.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Serialize the tow log to "disk" - for the day
     * @param ctx
     */
    protected void serialize(Context ctx) {
        String absolutePath = getFileNameForTheDay(ctx);
        serialize(mtowLogs, absolutePath);
    }

    /**
     * Serialize any tow log arraylist
     * @param towLogs
     * @param absolutePath  Where we want to store it
     */
    protected void serialize(ArrayList<TowLog> towLogs, String absolutePath) {
        final File suspend_f = new File(absolutePath);

        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(suspend_f);
            oos = new ObjectOutputStream(fos);

            oos.writeObject(towLogs);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (oos != null) oos.close();
                if (fos != null) fos.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Deserialize the current days tow logs
     * @param ctx
     */
    protected void deserialize(Context ctx) {
        String absolutePath = getFileNameForTheDay(ctx);
        mtowLogs = deserialize(absolutePath);
    }

    /**
     * Deserialize any tow log data at the given location
     * @param absolutePath  The absolute path
     * @return
     */
    private ArrayList<TowLog> deserialize(String absolutePath)
    {
        final File suspend_f = new File(absolutePath);
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(suspend_f);
            ois = new ObjectInputStream(fis);
            return (ArrayList<TowLog>) ois.readObject();
        } catch (Exception e) {
            return new ArrayList<TowLog>();     // Hack to initialize on error
        } finally {
            try {
                if (ois != null) ois.close();
                if (fis != null) fis.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * We build a file name based on the current day
     * This saves the tow pilot having to clear out the tow pilot log
     * @param ctx
     * @return
     */
    public String getFileNameForTheDay(Context ctx) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String dateExtension = format.format(new Date());
        File f = ctx.getFilesDir();
        return f.getAbsoluteFile() + File.separator + serializationName + dateExtension;
    }

    /**
     * As the tow logs are stored by unique day we can get a list by examining the storage
     * and matching on the root of the file name
     */
    public File[] getTowLogHistory(Context ctx) {
        File f = ctx.getFilesDir();
        File[] history = f.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return (filename.startsWith(serializationName));
            }
        });
        return history;
    }

    public ArrayList<TowLog> getTowLogsUsingAbsolutePath(Context ctx, String absolutePath) {
        ArrayList<TowLog> results = new ArrayList<TowLog>();
        File[] files = getTowLogHistory(ctx);
        for (File file : files) {
            if(absolutePath.equals(file.getAbsolutePath()))
            {
                results =  deserialize(absolutePath);
                break;
            }
         }
        return results;
    }

    /**
     * Convert TowLog ArrayList to JSON
     * @return JSON
     */
    protected JSONArray mapItemsToJSONArray(boolean isTest)
    {
        return mapItemsToJSONArray(mtowLogs, isTest);
    }

    protected JSONArray mapItemsToJSONArray(ArrayList<TowLog> towLogs, boolean isTest)
    {
        JSONArray results = new JSONArray();
        for (int i=0; i < towLogs.size(); i++)
        {
            JSONObject result =  mapItemToJSONObject(towLogs.get(i), isTest);
            results.put(result);
        }
        return results;
    }

    /**
     * Gets a single row as JSON
     * @param entry A TowList object
     * @return A JSON object
     */
    protected JSONObject mapItemToJSONObject(TowLog entry, boolean isTest)
    {
        JSONObject result = new JSONObject();
        try {
            result.put("tow_plane_id", entry.getTowPlaneId());
//            result.put("tow_plane_code", entry.getTowPlaneCode());

            result.put("tow_pilot_id", entry.getTowPilotId());
//            result.put("tow_pilot_code", entry.getTowPilotCode());

            result.put("airport_id", entry.getAirportId());
            result.put("airport_code", entry.getAirportCode());

            result.put("glider_id", entry.getGliderId());
//            result.put("glider_code", entry.getGliderCode());

            result.put("pilot1_id", entry.getPilotId());
//            result.put("pilot1_code", entry.getPilotCode());

            result.put("pilot2_id", 0);                 // App can't provide a pilot2!

            result.put("release_alt", entry.getReleaseAltitude());
            result.put("takeoff_alt", entry.getStartAltitude());

            // Output date/times in milliseconds
//            result.put("tow_start_millis", entry.getTowStartMillis());
//            result.put("tow_end_millis", entry.getTowEndMillis());
//            result.put("tow_max_millis", entry.getTowMaxMillis());

            // Output in local date format
            result.put("tow_start", DateUtility.GetDateAsLocal(entry.getTowStartMillis()));
            result.put("tow_end", DateUtility.GetDateAsLocal(entry.getTowEndMillis()));
            result.put("tow_release", DateUtility.GetDateAsLocal(entry.getTowMaxMillis()));

            result.put("unique_id", entry.getUniqueID());
            // Are we in test mode
            result.put("save", !isTest);
        } catch (JSONException e) {
            Log.e("JSONException: ", e.getMessage());
        }
        return result;
    }

    public void doUpLoadToServer(Context ctx, Boolean isTestMode) {
        deserialize(ctx);
        if (!isEmpty()) {
            JSONArray results = mapItemsToJSONArray(isTestMode);
            String json = results.toString();
            JSONPush upload = new JSONPush(ctx, uploadURL, json);
            upload.execute();
        }
    }
}
