package com.paragontek.barry.TowFlightLog;

import android.app.Application;
import android.content.Context;

/**
 * Created by Brendan on 4/29/15.
 */

//Link to create android global variables: http://stackoverflow.com/questions/1944656/android-global-variable

public class TowPlaneApplication extends Application {

    private static PilotData pilotData = null;
    private static GliderData gliderData = null;
    private static TowPlaneData towPlaneData = null;
    private static TowLogData towLogData = null;
    private static AirFieldData airFieldData = null;
    private BufferXYZ bufferStabilityXYZ = null;
    private BufferDouble bufferAltitude = null;
    private static BookingData bookingData = null;

    private static TowPlaneState towPlaneState = null;

    private static int m_speedThresholdStartTow = 5;            // Walking speed
    private static double m_startThresholdXYZ = 15.0;           // Changed needed for XYZ
    private static double m_startThresholdAltitude = .5;        // Changed needed for altitude
    private static final int BUFFERSIZE = 4;                    // Number of points to hold


    private static TowLog currentTowLog = null;

    public TowPlaneApplication() {
        pilotData = new PilotData();
        gliderData = new GliderData();
        towPlaneData = new TowPlaneData();
        towPlaneData = new TowPlaneData();
        airFieldData = new AirFieldData();
        bookingData = new BookingData();

        towPlaneState = new TowPlaneState();

        currentTowLog = new TowLog();

        bufferStabilityXYZ = new BufferXYZ(BUFFERSIZE, m_startThresholdXYZ);
        bufferAltitude = new BufferDouble(BUFFERSIZE, m_startThresholdAltitude);
    }

    public static boolean isEmpty() {
        return pilotData == null || gliderData == null || towPlaneData == null ||
                getPilotData().isEmpty() || getGliderData().isEmpty() || getTowPlaneData().isEmpty();
    }

    public static PilotData getPilotData() {
        return pilotData;
    }

    public static void setPilotData(PilotData pilotData) {
        TowPlaneApplication.pilotData = pilotData;
    }

    public static GliderData getGliderData() {
        return gliderData;
    }

    public static void setGliderData(GliderData gliderData) {
        TowPlaneApplication.gliderData = gliderData;
    }

    public static TowPlaneData getTowPlaneData() {
        return towPlaneData;
    }

    public static void setTowPlaneData(TowPlaneData towPlaneData) {
        TowPlaneApplication.towPlaneData = towPlaneData;
    }

    public static TowLogData getTowLogData() {
        return towLogData;
    }

    public static void setTowLogData(TowLogData towLogData) {
        TowPlaneApplication.towLogData = towLogData;
    }

    public static BookingData getBookingData() {
        return bookingData;
    }

    public static void setBookingData(BookingData bookingData) {
        TowPlaneApplication.bookingData = bookingData;
    }

    public static AirFieldData getAirFieldData() {
        return airFieldData;
    }

    public static void setAirFieldData(AirFieldData airFieldData) {
        TowPlaneApplication.airFieldData = airFieldData;
    }

    public static int getSpeedThresholdStartTow() {
        return m_speedThresholdStartTow;
    }

    public static void setSpeedThresholdStartTow(int speedThresholdStartTow) {
        TowPlaneApplication.m_speedThresholdStartTow = speedThresholdStartTow;
    }

    public static TowPlaneState getTowPlaneState() {
        return towPlaneState;
    }

    public static void setTowPlaneState(TowPlaneState towPlaneState) {
        TowPlaneApplication.towPlaneState = towPlaneState;
    }

    public static TowLog getCurrentTowLog() {
        return currentTowLog;
    }

    public static void setCurrentTowLog(TowLog currentTowLog) {
        TowPlaneApplication.currentTowLog = currentTowLog;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    //Check for null condition, and initialize if there is no tow log.
    //Post Condition: A serialized tow log record has been added
    public void serializeTowLog(Context ctx) {
        towLogData.serialize(getApplicationContext());
    }

    public void deSerializeTowLogData(Context ctx) {
        if (towLogData == null) {
            towLogData = new TowLogData();
        }
        towLogData.deserialize(ctx);
//        TowPlaneApplication.setTowLogData(towLogData);
    }

    public BufferXYZ getBufferStabilityXYZ() {
        return bufferStabilityXYZ;
    }

    public void setBufferStabilityXYZ(BufferXYZ bufferStabilityXYZ) {
        this.bufferStabilityXYZ = bufferStabilityXYZ;
    }

    public BufferDouble getBufferAltitude() {
        return bufferAltitude;
    }

    public void setBufferAltitude(BufferDouble bufferAltitude) {
        this.bufferAltitude = bufferAltitude;
    }
}
