package com.paragontek.barry.TowFlightLog;

import android.location.Location;

/**
 * Created by cbarry on 8/16/2015.
 * As described in: http://stackoverflow.com/questions/13323880/pass-data-from-class-to-activity
 */
public interface GPSInterface {
    void onGPSMessageReceived(String message);
    void onGPSAltitudeReceived(double altitudeAsMeters, double smoothedAltitudeAsMeters, Location location);
    void onGPSSpeedReceived(double speedAsMetersPerSecond, double smoothSpeedAsMetersPerSecond, Location location);
    void onGPSSatelliteStatusEvent(int inView, int inFix);
}
