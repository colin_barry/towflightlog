package com.paragontek.barry.TowFlightLog;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Brendan on 5/1/15.
 */
public class PilotData implements Serializable {

    private ArrayList<Pilot> pilots = new ArrayList<Pilot>();
    private static final String serializationName = "PilotData";

    public PilotData() {
    }

    public PilotData(ArrayList<Pilot> pilots) {
        this.pilots = pilots;
    }

    public ArrayList<Pilot> getPilots() {
        return pilots;
    }

    /**
     * Tow pilots are a subset of pilots
     *
     * @return
     */
    public ArrayList<Pilot> getTowPilots() {
        ArrayList<Pilot> towPilots = new ArrayList<Pilot>(pilots.size());
        for (Pilot pilot : pilots) {
            if (pilot.getIsTowPilot()) {
                towPilots.add(pilot);
            }
        }
        return towPilots;
    }

    public void setPilots(ArrayList<Pilot> pilots) {
        this.pilots = pilots;
    }

    public boolean isEmpty() {
        return pilots == null || pilots.size() == 0;
    }

    protected Pilot findInstanceByCode(String code) {
        int pos = findPositionByCode(code);
        return pos > 0 ? pilots.get(pos) : null;
    }

    protected Pilot findInstanceByID(int id) {
        if (!this.pilots.isEmpty()) {
            for (Pilot item : pilots) {
                if (item.getId() == id)
                    return item;
            }
        }
        return null;
    }

    protected int findPositionByCode(String code) {
        if (!this.pilots.isEmpty()) {
            for (int i = 0; i < this.pilots.size(); i++) {
                if (this.pilots.get(i).getCode().equals(code)) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Set the filter sequence back to 0, effectively leaving the pilot list in its orginal sorted state
     */
    protected void initializeFilterSequence() {
        if (!this.pilots.isEmpty()) {
            for (Pilot item : pilots) {
                item.setFilterSequence(0);
            }
        }
    }

    protected void serialize(Context ctx) {
        File f = ctx.getFilesDir();
        final File suspend_f = new File(f.getAbsoluteFile() + File.separator + serializationName);

        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = new FileOutputStream(suspend_f);
            oos = new ObjectOutputStream(fos);

            oos.writeObject(pilots);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (oos != null) oos.close();
                if (fos != null) fos.close();
            } catch (Exception e) {
            }

        }
    }

    public void deserialize(Context ctx) {
        File f = ctx.getFilesDir();
        final File suspend_f = new File(f.getAbsoluteFile() + File.separator + serializationName);

        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(suspend_f);
            ois = new ObjectInputStream(fis);
            pilots = (ArrayList<Pilot>) ois.readObject();
        } catch (Exception e) {
        } finally {
            try {
                if (ois != null) ois.close();
                if (fis != null) fis.close();
            } catch (Exception e) {
            }
        }
    }
}

