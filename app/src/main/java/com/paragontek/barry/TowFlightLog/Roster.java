package com.paragontek.barry.TowFlightLog;

/**
 *
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 *
 *
 * Class to hold roster objects
 */
public class Roster {
    private String code;
    private String firstName;
    private String lastName;
    private String name;
    private Boolean isTowPilot;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getIsTowPilot() {
        return isTowPilot;
    }

    public void setIsTowPilot(Boolean isTowPilot) {
        this.isTowPilot = isTowPilot;
    }

    @Override
    public String toString() {
        return "pilotCode: " + code + "\tLastName: " + lastName;
     }
}
