package com.paragontek.barry.TowFlightLog;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 */
public class GliderJSON extends JSONPull {

    private static final String URL = "http://ssb.michirado.com/flightlog/__gliders.json";

    private static final String TAG_ID = "id";
    private static final String TAG_CODE = "name";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_IMAGE_URI = "image_url";
    private static final String TAG_IS_TWIN = "is_twin";
    private static final String TAG_IS_ACTIVE = "is_active";

    private ArrayList<Glider> Gliders = new ArrayList<Glider>();

    public GliderJSON(Context ctx) {
        super(ctx, URL);
    }

    @Override
    protected void onPostExecute(JSONArray jArray) {
        super.onPostExecute(jArray);
        try {
            // Convert jArray to a collection
            if (jArray != null && jArray.length() > 0) {
                int siz = jArray.length();
                Gliders = new ArrayList<Glider>(siz);
                for (int i = 0; i < siz; i++) {
                    JSONObject obj = jArray.getJSONObject(i);
                    Glider glider = new Glider();

                    int id = obj.getInt(TAG_ID);
                    String code = obj.getString(TAG_CODE);
                    String description = obj.getString(TAG_DESCRIPTION);

                    String imageUri = null;
                    int isTwin = obj.getInt(TAG_IS_TWIN);
                    int isActive = obj.getInt(TAG_IS_ACTIVE);

                    if (obj.has(TAG_IMAGE_URI)) {
                        imageUri = obj.getString(TAG_IMAGE_URI);
                    }

                    glider.setId(id);
                    glider.setCode(code);
                    glider.setDescription(description);
                    glider.setIsTwin(isTwin == 1);
                    glider.setIsActive(isActive == 1);
                    glider.setImageUri(imageUri);

                    Gliders.add(glider);            // Add to collection
                }

                GliderData gliderData = new GliderData();
                gliderData.setGliders(Gliders);
                gliderData.serialize(ctx);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected ArrayList<Glider> getGliders() {
        return Gliders;
    }

    protected Glider findGliderByCode(String code) {
        if (!this.Gliders.isEmpty() || !code.isEmpty()) {
            for (int i = 0; i < this.Gliders.size(); i++) {
                Glider temp = this.Gliders.get(i);
                if (temp.getCode().equals(code)){
                    return Gliders.get(i);
                }
            }
        }
        return null;
    }
}
