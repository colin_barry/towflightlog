package com.paragontek.barry.TowFlightLog;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 *
 * This Class implemented based on the following authors code
 * Author: Francesco Noya (francesco.noya AT studenti.unimi.it)
 * http://www.anddev.org/write_a_simple_xml_file_in_the_sd_card_using_xmlserializer-t8350.html
 */


public class TowLogXML extends AppCompatActivity {

    private static final String TAG_TOW_LOG = "towlog";
    private static final String TAG_ROOT = "root";
    private static final String TAG_PILOT_ID = "pilotid";
    private static final String TAG_PILOT_CODE = "pilotcode";
    private static final String TAG_GLIDER_ID = "gliderid";
    private static final String TAG_GLIDER_CODE = "glidercode";
    private static final String TAG_HEIGHT = "height";
    ArrayList<TowLog> towLogs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow_log_xml);

        TowPlaneApplication app = ((TowPlaneApplication) this.getApplication());
        towLogs = TowPlaneApplication.getTowLogData().getTowLogs();


        final Button btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToXML();
            }
        });

        final Button btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getTowLogIntent = new Intent(getApplicationContext(), TowLogListView.class);
                TowLogXML.this.startActivity(getTowLogIntent);
            }
        });
    }

    public void saveToXML(){
        //create a new file called "new.xml" in the SD card
        File newxmlfile = new File(Environment.getExternalStorageDirectory() + "/new.xml");
        try {
            newxmlfile.createNewFile();
        } catch (IOException e) {
            Log.e("IOException", "exception in createNewFile() method");
        }
        //we have to bind the new file with a FileOutputStream
        FileOutputStream fileos = null;
        try {
            fileos = new FileOutputStream(newxmlfile);
        } catch (FileNotFoundException e) {
            Log.e("FileNotFoundException", "can't create FileOutputStream");
        }
        //we create a XmlSerializer in order to write xml data
        XmlSerializer serializer = Xml.newSerializer();
        try {
            //we set the FileOutputStream as output for the serializer, using UTF-8 encoding
            serializer.setOutput(fileos, "UTF-8");
            //Write <?xml declaration with encoding (if encoding not null) and standalone flag (if standalone not null)
            serializer.startDocument(null, Boolean.valueOf(true));
            //set indentation option
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            //start a tag called "root"


            serializer.startTag(null, TAG_ROOT);
            //i indent code just to have a view similar to xml-tree
            for (TowLog towLog : towLogs) {
                serializer.startTag(null, TAG_TOW_LOG);
                serializer.attribute(null, TAG_PILOT_ID, String.valueOf(towLog.getPilotId()));
                serializer.attribute(null, TAG_PILOT_CODE, (towLog.getPilotCode()));
                serializer.attribute(null, TAG_GLIDER_ID, String.valueOf(towLog.getGliderId()));
                serializer.attribute(null, TAG_GLIDER_CODE, (towLog.getGliderCode()));
                serializer.attribute(null, TAG_HEIGHT, String.valueOf(towLog.getReleaseAltitude()));
                serializer.endTag(null, TAG_TOW_LOG);
            }

            serializer.endTag(null, TAG_ROOT);
            serializer.endDocument();
            //write xml data into the FileOutputStream
            serializer.flush();
            //finally we close the file stream
            fileos.close();

//            TextView tv = (TextView)this.findViewById(R.id.result);
//            tv.setText("file has been created on SD card");
        } catch (Exception e) {
            Log.e("Exception", "error occurred while creating xml file");
        }
        String message = String.format("You serialized %d records", towLogs.size());
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_return_to_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_main) {
            Intent mainIntent = new Intent(this, MainActivity.class);
            TowLogXML.this.startActivity(mainIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
