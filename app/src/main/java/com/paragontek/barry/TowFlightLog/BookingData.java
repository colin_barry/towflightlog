package com.paragontek.barry.TowFlightLog;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by cbarry on 12/10/2015.
 */
public class BookingData {
    private ArrayList<Booking> bookings = new ArrayList<Booking>();

    private static final String serializationName = "BookingData";

    public BookingData() {
    }

    public BookingData(ArrayList<Booking> bookings) {
        this.bookings = bookings;
    }

    public ArrayList<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(ArrayList<Booking> bookings) {
        this.bookings = bookings;
    }

    public boolean isEmpty() {
        return bookings == null || bookings.size() == 0;
    }

    protected Booking findBookingByCode(String code) {
        if (!this.bookings.isEmpty()) {
            for (int i = 0; i < this.bookings.size(); i++) {
                if (this.bookings.get(i).getCode().equals(code)) {
                    return bookings.get(i);
                }
            }
        }
        return null;
    }

    protected int findPositionByCode(String code) {
        if (!this.bookings.isEmpty()) {
            for (int i = 0; i < this.bookings.size(); i++) {
                if (this.bookings.get(i).getCode().equals(code)) {
                    return i;
                }
            }
        }
        return -1;
    }


    protected void serialize(Context ctx) {
        File f = ctx.getFilesDir();
        final File suspend_f = new File(f.getAbsoluteFile() + File.separator + serializationName);

        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = new FileOutputStream(suspend_f);
            oos = new ObjectOutputStream(fos);

            oos.writeObject(bookings);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (oos != null) oos.close();
                if (fos != null) fos.close();
            } catch (Exception e) {
            }
        }
    }

    public void deserialize(Context ctx) {
        File f = ctx.getFilesDir();
        final File suspend_f = new File(f.getAbsoluteFile() + File.separator + serializationName);

        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(suspend_f);
            ois = new ObjectInputStream(fis);
            bookings = (ArrayList<Booking>) ois.readObject();
        } catch (Exception e) {
        } finally {
            try {
                if (ois != null) ois.close();
                if (fis != null) fis.close();
            } catch (Exception e) {
            }
        }
    }
}
