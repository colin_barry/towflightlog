package com.paragontek.barry.TowFlightLog;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;



/**
 * Created by cbarry on 9/12/2015.
 * based on http://stackoverflow.com/questions/6493517/detect-if-android-device-has-internet-connection/6493572#6493572
 */
public class WifiTracker extends AsyncTask<String, Integer, String> {

    private Context context;
    private ProgressDialog dialog;
    private boolean hasAccess = false;
    private String SSID;
    private AsyncTaskCompleted listener;


    /**
     * Constructor for this type
     *
     * @param context The ctx
     */
    public WifiTracker(Context context, AsyncTaskCompleted listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage("Checking wifi...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(true);
        dialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        getNetworkDetails();
        String message = hasAccess ? "Has Wifi Access" : "No Wifi Access";
        return message;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        listener.asyncTaskCompleted();  // Tell listeners we are complete
        dialog.dismiss();
    }

    protected void getNetworkDetails() {
        hasAccess = false;
        if (isNetworkAvailable()) {
            getSSIDDetails();
            try {
                HttpURLConnection urlc = (HttpURLConnection)
                        (new URL("http://clients3.google.com/generate_204").openConnection());
                urlc.setRequestProperty("User-Agent", "Android");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                hasAccess= (urlc.getResponseCode() == 204 && urlc.getContentLength() == 0);
            } catch (IOException e) {
                Log.e("MyApp", "Error checking internet connection", e);
            }
        } else {
            Log.i("MyApp", "No network available!");
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void getSSIDDetails()
    {
        SSID = "";
        WifiManager wifiManager = (WifiManager) this.context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager != null) {
            WifiInfo info = wifiManager.getConnectionInfo();
            if (info != null) {
                SSID  = info.getSSID();
            }
        }
    }


    /**
     * Attribute to return network connectivity
     *
     * @return
     */
    protected boolean hasInternetAccess() {
        return hasAccess;
    }

    protected String getSSID() {
        return SSID;
    }
}
