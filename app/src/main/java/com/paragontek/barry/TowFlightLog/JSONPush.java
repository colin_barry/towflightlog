package com.paragontek.barry.TowFlightLog;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.SystemClock;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by cbarry on 2/15/2016.
 * Class to upload JSON data to an endpoint
 */
public class JSONPush extends AsyncTask<String, Void, Void> {

    protected final Context ctx;
    ProgressDialog dialog;
    private String url;
    private String json;

    public JSONPush(Context ctx, String url, String json) {
        this.ctx = ctx;
        this.url = url;
        this.json = json;
    }

    @Override
    protected Void doInBackground(String... params) {
//        SystemClock.sleep(2000);
//        return (null);
        makeRequest();
        return (null);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(ctx);
        dialog.setMessage("Pushing Data ...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(true);
        dialog.show();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        dialog.dismiss();
    }

//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        dialog = new ProgressDialog(ctx);
//        dialog.setMessage("Uploading Data ...");
//        dialog.setIndeterminate(false);
//        dialog.setCancelable(true);
//        dialog.show();
//    }
//
//    @Override
//    protected Void doInBackground(String... params) {
//        SystemClock.sleep(2000);
//        return (null);
//
////        String result;
//////        try {
//////            result = makeRequest();
//////        } catch (IOException e) {
//////            e.printStackTrace();
//////        }
////        return result;
//    }
//
//    @Override
//    protected void onPostExecute(){
//        dialog.dismiss();
//    }

//    public void execute(String json) {
//        makeRequest("http://ssb.michirado.com/flightlog/__upload_towlog_data", json);
//    }

    public String makeRequest() {
        String result = null;
        HttpURLConnection conn = null;
        OutputStream os = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        try{
//Connect
            conn = (HttpURLConnection) ((new URL (url).openConnection()));
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            conn.connect();

//Write
            os = conn.getOutputStream();
            writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(json);

//Read back response??
            br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));

            String line = null;
            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            result = sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) br.close();
                if (writer != null) writer.close();
                if (os != null) os.close();
                if (conn != null) conn.disconnect();
            } catch (Exception e) {
                // Eat exception
            }
        }
        return result;
    }
}
