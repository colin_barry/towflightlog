package com.paragontek.barry.TowFlightLog;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by cbarry on 12/10/2015.
 */
public class BookingJSON extends JSONPull {

    private static final String URL = "http://ssb.michirado.com/flightlog/__today.json";

    private static final String TAG_ID = "id";
    private static final String TAG_PILOT1 = "pilot1_id";
    private static final String TAG_PILOT2 = "pilot2_id";
    private static final String TAG_GLIDER = "glider_id";

    private ArrayList<Booking> Bookings = new ArrayList<Booking>();

    public BookingJSON(Context ctx) {
        super(ctx, URL);
    }

    @Override
    protected void onPostExecute(JSONArray jArray) {
        super.onPostExecute(jArray);
        try {
            // Convert jArray to a collection
            if (jArray != null && jArray.length() > 0) {
                int siz = jArray.length();
                Bookings = new ArrayList<Booking>(siz);
                for (int i = 0; i < siz; i++) {
                    JSONObject obj = jArray.getJSONObject(i);
                    Booking booking = new Booking();

                    int id = obj.getInt(TAG_ID);
                    int pilot1ID = obj.getInt(TAG_PILOT1);
                    int pilot2ID = obj.getInt(TAG_PILOT2);
                    int gliderID = obj.getInt(TAG_GLIDER);

                    booking.setId(id);
                    booking.setPilot1ID(pilot1ID);
                    booking.setPilot2ID(pilot2ID);
                    booking.setGliderID(gliderID);

                    Bookings.add(booking);            // Add to collection
                }

                BookingData bookingData = new BookingData();
                bookingData.setBookings(Bookings);
                bookingData.serialize(ctx);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    BookingData bookingData = new BookingData();
//    ArrayList<Booking> bookings = bookingJSON.getBookings();
//    bookingData.setBookings(bookings);
//    bookingData.serialize(GetData.this);
//    TowPlaneApplication.setBookingData(bookingData);


    protected ArrayList<Booking> getBookings() {
        return Bookings;
    }

    protected Booking findBookingByCode(String code) {
        if (!this.Bookings.isEmpty() || !code.isEmpty()) {
            for (int i = 0; i < this.Bookings.size(); i++) {
                Booking temp = this.Bookings.get(i);
                if (temp.getCode().equals(code)){
                    return Bookings.get(i);
                }
            }
        }
        return null;
    }
}
