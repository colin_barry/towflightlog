package com.paragontek.barry.TowFlightLog;

import java.util.ArrayList;

/**
 * Created by Brendan on 5/7/15.
 */
public class AirFieldData {

    public ArrayList<AirField> getAirFields() {
        return airFields;
    }

    public void setAirFields(ArrayList<AirField> airFields) {
        this.airFields = airFields;
    }

    ArrayList<AirField> airFields = null;

    //Hardwired with only two airports Boulder & Salida
    public AirFieldData() {
        airFields = new ArrayList<AirField>();
        airFields.add(new AirField(1, "KBDU", 5290));
        airFields.add(new AirField(2, "KANK", 7523));
        airFields.add(new AirField(3, "TEST", 1234));
    }

    protected AirField findInstanceByCode(String code){
        int pos = findPositionByCode(code);
        return pos >= 0 ? airFields.get(pos) : null;
    }

    protected int findPositionByCode(String code){
        if (!this.airFields.isEmpty()) {
            for (int i = 0; i < this.airFields.size(); i++) {
                if (this.airFields.get(i).getAirPortCode().equals(code)) {
                    return i;
                }
            }
        }
        return -1;
    }
}
