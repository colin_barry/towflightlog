package com.paragontek.barry.TowFlightLog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 */
public class GliderSpinnerAdapter extends ArrayAdapter<Glider> {

    private Context ctx;
    private ArrayList<Glider> values;

    private int spinnerLayoutID;

    public int getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
    }

    private int selectedItem = 0;

    public GliderSpinnerAdapter(Context ctx, int spinnerLayoutID, ArrayList<Glider> objects) {
        super(ctx, spinnerLayoutID, objects);
        this.ctx = ctx;
        this.values = objects;
        this.spinnerLayoutID = spinnerLayoutID;
    }

    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        return getCustomView(position, cnvtView, prnt);
    }

    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
        return getCustomView(pos, cnvtView, prnt);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View mySpinner = inflater.inflate(spinnerLayoutID, parent, false);

        String code = values.get(position).getCode();
        String description = values.get(position).getDescription();

        TextView tvCode = (TextView) mySpinner.findViewById(R.id.tvCode);
        tvCode.setText(code);

        TextView tvDescription = (TextView) mySpinner.findViewById(R.id.tvDescription);
        tvDescription.setText(description);

//        ImageView left_icon = (ImageView) mySpinner.findViewById(R.id.left_pic);
//        left_icon.setImageResource(R.drawable.abc_btn_check_to_on_mtrl_000);

        return mySpinner;
    }
}
