package com.paragontek.barry.TowFlightLog;

import android.util.Log;

//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 * <p/>
 * Class to attach to endpoint and fetch a JSON object
 */
public class JSONParser {

    public JSONParser() {
    }

    public JSONArray getJSONFromUrl(String url) throws IOException {
        HttpURLConnection conn = null;
        InputStream is = null;
        JSONArray jArray = null;

        // Making HTTP request
        try {
            conn = (HttpURLConnection) ((new URL(url).openConnection()));
            ;
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();

            int response = conn.getResponseCode();
            is = conn.getInputStream();

            String json = readInputStreamToJson(is);

            jArray = mapJsonStringToArray(json);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
                if (conn != null) conn.disconnect();
            } catch (Exception e) {
                // Eat exception
            }

        }
        return jArray;
    }

    private String readInputStreamToJson(InputStream is) throws IOException {
        String json = "";
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (Exception e) {
            Log.e("Buffer Error", "Error reading inputstream " + e.toString());
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return sb.toString();
    }

    /**
     * Try parse the string to a JSON Array
     *
     * @param json
     * @return
     */
    private JSONArray mapJsonStringToArray(String json) {
        JSONArray jArray = null;
        try {
            jArray = new JSONArray(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data to JSON Array" + e.toString());
        }
        return jArray;
    }
}
