package com.paragontek.barry.TowFlightLog;

/**
 * Created by cbarry on 9/12/2015.
 * based on comments in http://stackoverflow.com/questions/19520188/return-value-from-asynctask-without-get-method
 */
public interface AsyncTaskCompleted {
    void asyncTaskCompleted();
}


