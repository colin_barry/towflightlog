package com.paragontek.barry.TowFlightLog;

import android.util.Log;

/**
 * Created by cbarry on 9/23/2015.
 */
public class LowPassFilter {
    private int mSampleCount;
    private double mLastReading;
    private double mFactorA;
    private double mFactorB;
    private boolean mValueHasChanged;

    /**
     * Constructor for this type
     * @param Start The initial value
     * @param Factor The initial factor. Use this to adjust how quickly the low pas filter responds
     */
    public LowPassFilter(double Start, double Factor)
    {
        mSampleCount = 1;
        mLastReading = Start;
        mFactorA = 1.0 - Factor;
        mFactorB = Factor;
        mValueHasChanged = true;
     }

    /**
     * Set new value
     * @param R the value
     * @return The filtered value
     */
    double NewReading(double R)
    {
        mSampleCount++;
        double reading = (mFactorA * mLastReading) + (mFactorB * R);
        if (reading != mLastReading) {
            mValueHasChanged = true;
            mLastReading = reading;
        } else {
            mValueHasChanged = false;
        }
        return mLastReading;
    }

    /**
     * Get the last reading
     * @return The reading
     */
    public double getLastReading()
    {
        return mLastReading;
    }
}

