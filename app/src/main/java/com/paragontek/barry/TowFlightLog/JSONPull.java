package com.paragontek.barry.TowFlightLog;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONArray;

import java.io.IOException;

/**
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 * http://stackoverflow.com/questions/15719942/get-json-in-asynctask-android
 */
public class JSONPull extends AsyncTask<Void, String, JSONArray> {
    protected final Context ctx;
    ProgressDialog dialog;
    private String url;

    /*
     * Constructor so that we can pass in ctx and url
     */
    public JSONPull(Context ctx, String url) {
        this.ctx = ctx;
        this.url = url;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(ctx);
        dialog.setMessage("Pulling Data ...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(true);
        dialog.show();
    }

    @Override
    protected JSONArray doInBackground(Void... unused) {
//        SystemClock.sleep(2000);
//        return (null);
        JSONParser jParser = new JSONParser();
        // Getting JSON using URL

        JSONArray jArray = null;
        try {
            jArray = jParser.getJSONFromUrl(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jArray;
    }

    @Override
    protected void onPostExecute(JSONArray jArray){
        dialog.dismiss();
    }
}
