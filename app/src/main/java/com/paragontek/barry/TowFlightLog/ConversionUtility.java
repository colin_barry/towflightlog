package com.paragontek.barry.TowFlightLog;

/**
 * Created by cbarry on 9/22/2015.
 * Class for conversion of standard units
 */
public class ConversionUtility {

    /**
     * Conversion of meters to feet
     * @param meters The value in meters
     * @return The value in feet
     */
    public static int convertMetersToFeet(double meters)
    {
        //function converts Feet to Meters.
        return (int)Math.floor(meters*3.2808 + 0.5d);
    }

    /**
     * Conversion of feet to meters
     *
     * @param feet The value in feet
     * @return The value in meters
     */
    public static double convertFeetToMeters(int feet)
    {
        //function converts Feet to Meters.
        double toMeters = feet / 3.2808;
        return toMeters;
    }
}
