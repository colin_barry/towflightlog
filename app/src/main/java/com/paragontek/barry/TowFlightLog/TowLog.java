package com.paragontek.barry.TowFlightLog;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Brendan on 4/30/15.
 */
public class TowLog implements Serializable {
    private String pilotCode;
    private int pilotId;

    private String gliderCode;
    private int gliderId;

    private String towPlaneCode;
    private int towPlaneId;

    private String towPilotCode;
    private int towPilotId;

    private String airportCode;
    private int airportId;

    private int releaseAltitude;
    private int startAltitude;

    private long towStartMillis;
    private long towEndMillis;
    private long towMaxMillis;

    private String UniqueID;

    public TowLog() {
        //towStartMillis = DateUtility.GetDateAsUTCString();
        UniqueID = UUID.randomUUID().toString();
    }

    public TowLog(String pilotCode, int pilotId, String gliderCode, int gliderId, String towPlaneCode, int towPlaneId, int releaseAltitude, long towStartMillis, long towEndMillis, long towMaxMillis) {
        this.pilotCode = pilotCode;
        this.pilotId = pilotId;
        this.gliderCode = gliderCode;
        this.gliderId = gliderId;
        this.towPlaneCode = towPlaneCode;
        this.towPlaneId = towPlaneId;
        this.releaseAltitude = releaseAltitude;
        this.towStartMillis = towStartMillis;
        this.towEndMillis = towEndMillis;
        this.towMaxMillis = towMaxMillis;
    }

    public String getPilotCode() {
        return pilotCode;
    }

    public void setPilotCode(String pilotCode) {
        this.pilotCode = pilotCode;
    }

    public int getPilotId() {
        return pilotId;
    }

    public void setPilotId(int pilotId) {
        this.pilotId = pilotId;
    }

    public String getGliderCode() {
        return gliderCode;
    }

    public void setGliderCode(String gliderCode) {
        this.gliderCode = gliderCode;
    }

    public int getGliderId() {
        return gliderId;
    }

    public void setGliderId(int gliderId) {
        this.gliderId = gliderId;
    }

    public String getTowPlaneCode() {
        return towPlaneCode;
    }

    public void setTowPlaneCode(String towPlaneCode) {
        this.towPlaneCode = towPlaneCode;
    }

    public int getTowPlaneId() {
        return towPlaneId;
    }

    public void setTowPlaneId(int towPlaneId) {
        this.towPlaneId = towPlaneId;
    }

    public int getReleaseAltitude() {
        return releaseAltitude;
    }

    public void setReleaseAltitude(int releaseAltitude) {
        this.releaseAltitude = releaseAltitude;
    }

    public int getStartAltitude() {
        return startAltitude;
    }

    public void setStartAltitude(int startAltitude) {
        this.startAltitude = startAltitude;
    }

    public String getTowPilotCode() {
        return towPilotCode;
    }

    public void setTowPilotCode(String towPilotCode) {
        this.towPilotCode = towPilotCode;
    }

    public int getTowPilotId() {
        return towPilotId;
    }

    public void setTowPilotId(int towPilotId) {
        this.towPilotId = towPilotId;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public int getAirportId() {
        return airportId;
    }

    public void setAirportId(int airportId) {
        this.airportId = airportId;
    }

    public long getTowStartMillis() {
        return towStartMillis;
    }

    public void setTowStartMillis(long towStartMillis) {
        this.towStartMillis = towStartMillis;
    }

    public long getTowEndMillis() {
        return towEndMillis;
    }

    public void setTowEndMillis(long towEndMillis) {
        this.towEndMillis = towEndMillis;
    }

    public long getTowMaxMillis() {
        return towMaxMillis;
    }

    public void setTowMaxMillis(long towMaxMillis) {
        this.towMaxMillis = towMaxMillis;
    }

    public String getUniqueID() {
        return UniqueID;
    }

    public void UniqueID(String UniqueID) {
        this.UniqueID = UniqueID;
    }

    @Override
    public String toString() {
        return "TowLog{" +
                "pilotCode='" + pilotCode + '\'' +
                ", gliderCode='" + gliderCode + '\'' +
                ", towPlaneCode='" + towPlaneCode + '\'' +
                ", towPlaneId=" + towPlaneId +
                '}';
    }
}
