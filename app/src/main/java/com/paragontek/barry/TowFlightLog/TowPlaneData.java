package com.paragontek.barry.TowFlightLog;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Brendan on 5/1/15.
 */
public class TowPlaneData implements Serializable {
    private ArrayList<TowPlane> towPlanes = new ArrayList<TowPlane>();

    private static final String serializationName = "TowPlaneData";
    public TowPlaneData(){}

    public TowPlaneData(ArrayList<TowPlane> towPlanes){
        this.towPlanes = towPlanes;
    }

    public ArrayList<TowPlane> getTowPlanes() {
        return towPlanes;
    }

    public void setTowPlanes(ArrayList<TowPlane> towPlanes) {
        this.towPlanes = towPlanes;
    }

    public boolean isEmpty(){
        return towPlanes == null || towPlanes.size() == 0;
    }

    protected TowPlane findInstanceByCode(String code){
        int pos = findPositionByCode(code);
        return pos > 0 ? towPlanes.get(pos) : null;
    }

    protected int findPositionByCode(String code){
        if (!this.towPlanes.isEmpty()) {
            for (int i = 0; i < this.towPlanes.size(); i++) {
                if (this.towPlanes.get(i).getCode().equals(code)) {
                    return i;
                }
            }
        }
        return -1;
    }

    protected void serialize(Context ctx) {
        File f = ctx.getFilesDir();
        final File suspend_f = new File(f.getAbsoluteFile() + File.separator + serializationName);

        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = new FileOutputStream(suspend_f);
            oos = new ObjectOutputStream(fos);

            oos.writeObject(towPlanes);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (oos != null) oos.close();
                if (fos != null) fos.close();
            } catch (Exception e) {
            }

        }
    }

    public void deserialize(Context ctx) {
        File f = ctx.getFilesDir();
        final File suspend_f = new File(f.getAbsoluteFile() + File.separator + serializationName);

        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(suspend_f);
            ois = new ObjectInputStream(fis);
            towPlanes = (ArrayList<TowPlane>) ois.readObject();
        } catch (Exception e) {
        } finally {
            try {
                if (ois != null) ois.close();
                if (fis != null) fis.close();
            } catch (Exception e) {
            }
        }
    }
}
