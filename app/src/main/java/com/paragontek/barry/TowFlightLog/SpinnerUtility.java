package com.paragontek.barry.TowFlightLog;

import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;

/**
 * Created by cbarry on 12/5/2015.
 */
public class SpinnerUtility {

    /**
     * Sets the pilot spinner to the given code
     * @param pilotSpinner      The spinner object
     * @param pilotCode         The code we want to set the spinner to
     */
    public static void selectPilotSpinnerItemByCode(Spinner pilotSpinner, String pilotCode)
    {
        for (int position = 0; position < pilotSpinner.getCount(); position++)
        {
            Pilot item =  (Pilot) pilotSpinner.getItemAtPosition(position);
            if (item.getCode().equals(pilotCode)) {
                pilotSpinner.setSelection(position);
                return;
            }
        }
        pilotSpinner.setSelection(-1);
    }

    /**
     * Sets the pilot spinner to the given code
     * @param pilotSpinner      The spinner object
     * @param pilotID           The pilot ID to set the spinner to
     */
    public static void selectPilotSpinnerItemByID(Spinner pilotSpinner, int pilotID)
    {
        for (int position = 0; position < pilotSpinner.getCount(); position++)
        {
            Pilot item =  (Pilot) pilotSpinner.getItemAtPosition(position);
            if (item.getId() == pilotID) {
                pilotSpinner.setSelection(position);
                return;
            }
        }
        pilotSpinner.setSelection(-1);
    }

    /**
     * Sets the glider spinner to the given code
     * @param gliderSpinner     The spinner object
     * @param gliderCode        The code we want to set the spinner to
     */
    public static void selectGliderSpinnerItemByCode(Spinner gliderSpinner, String gliderCode)
    {
        for (int position = 0; position < gliderSpinner.getCount(); position++)
        {
            Glider item = (Glider) gliderSpinner.getItemAtPosition(position);
            if (item.getCode().equals(gliderCode)) {
                gliderSpinner.setSelection(position);
                return;
            }
        }
        gliderSpinner.setSelection(-1);
    }

    /**
     * Sets the glider spinner to the given code
     * @param gliderSpinner      The spinner object
     * @param gliderID           The glider ID to set the spinner to
     */
    public static void selectGliderSpinnerItemByID(Spinner gliderSpinner, int gliderID)
    {
        for (int position = 0; position < gliderSpinner.getCount(); position++)
        {
            Glider item =  (Glider) gliderSpinner.getItemAtPosition(position);
            if (item.getId() == gliderID) {
                gliderSpinner.setSelection(position);
                return;
            }
        }
        gliderSpinner.setSelection(-1);
    }
}
