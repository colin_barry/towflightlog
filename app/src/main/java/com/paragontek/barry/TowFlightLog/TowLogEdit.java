package com.paragontek.barry.TowFlightLog;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 */
public class TowLogEdit extends AppCompatActivity {

    TowPlaneApplication app = null;
    TowLogData towLogData = null;
    Spinner gliderSpinner = null;
    Spinner pilotSpinner = null;
    Spinner towpilotSpinner = null;
    GliderSpinnerAdapter gliderSpinnerAdapter = null;
    PilotSpinnerAdapter pilotSpinnerAdapter = null;
    PilotSpinnerAdapter towpilotSpinnerAdapter = null;
    TextView txtTowHeight = null;
    AirField airField = null;

    int positionOfRecord = -1;
    String absolutePath = null;
    ArrayList<TowLog> towLogs = null;

    NumberPicker[] nps=new NumberPicker[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow_log_edit);

        app = ((TowPlaneApplication) this.getApplication());
        towLogData = app.getTowLogData();

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            positionOfRecord = extras.getInt("Position");
            absolutePath = extras.getString("LogDataAbsolutePath");
            towLogs = (ArrayList<TowLog>) extras.getSerializable("TowLogsCollection");
        }

        TowLog towLog = towLogs.get(positionOfRecord);

        //We currently only handle one airport but have left the ability to have multiple airfields in the app
        AirFieldData airFieldData = new AirFieldData();
        airField = airFieldData.getAirFields().get(0);

        //Set up Spinners/Pickers and initiliaze position to selected record data
        setUpGliderSpinner(towLog.getGliderId());
        setUpPilotSpinner(towLog.getPilotId());
        setUpTowPilotSpinner(towLog.getTowPilotId());

        int towHeight = towLog.getReleaseAltitude();
        setUpNumberPickers(towHeight);

        final Button btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUpdatedTowLog();
            }
        });

        final Button btnDelete = (Button) findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCurrentTowLog();
            }
        });

        final Button btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToTowLogListViewActivity();
            }
        });

        // Stop soft keyboard from showing automatically
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void saveUpdatedTowLog() {

        TowLog towLog = new TowLog();

        Glider glider = (Glider) gliderSpinner.getSelectedItem();
        if (glider == null || glider.getId() == -1) {
            Toast.makeText(getApplicationContext(), "Glider code invalid.", Toast.LENGTH_SHORT).show();
            return;
        }

        String gliderName = glider.getCode();
        towLog.setGliderCode(gliderName);
        towLog.setGliderId(glider.getId());

        Pilot pilot = (Pilot) pilotSpinner.getSelectedItem();
        if (pilot == null || pilot.getId() == -1) {
            Toast.makeText(getApplicationContext(), "Pilot code invalid.", Toast.LENGTH_SHORT).show();
            return;
        }

        String pilotCode = pilot.getCode();
        String pilotName = pilot.getName();
        towLog.setPilotCode(pilotCode);
        towLog.setPilotId(pilot.getId());

        Pilot towPilot = (Pilot) towpilotSpinner.getSelectedItem();
        if (pilot == null || pilot.getId() == -1) {
            Toast.makeText(getApplicationContext(), "Tow Pilot code invalid.", Toast.LENGTH_SHORT).show();
            return;
        }
        towLog.setTowPilotCode(towPilot.getCode());
        towLog.setTowPilotId(towPilot.getId());

        int height;
        height = Integer.parseInt(txtTowHeight.getText().toString());
        if(height < airField.getMsl())
        {
            Toast.makeText(getApplicationContext(), "Supplied tow height is less than airfield msl", Toast.LENGTH_SHORT).show();
            return;
        }
        towLog.setReleaseAltitude(height);

        doUpdate(towLog, positionOfRecord);

        returnToTowLogListViewActivity();
    }

    /**
     *
     * Replace the current record and reserialize the data
     *
     * @param updatedTowLog The updated TowLog data model
     * @param positionOfRecord The location for the update to be stored
     */
    private void doUpdate(TowLog updatedTowLog, int positionOfRecord)
    {
        towLogs.set(positionOfRecord, updatedTowLog);
        towLogData.serialize(towLogs, absolutePath);
    }

    /**
     * Returns to the tow log list view activity
     */
    private void returnToTowLogListViewActivity()
    {
        Intent intent = new Intent(getApplicationContext(), TowLogListView.class);
        intent.putExtra("LogDataAbsolutePath", absolutePath);
        TowLogEdit.this.startActivity(intent);
    }

    /**
     * Delete the current tow log
     */
    private void deleteCurrentTowLog() {
        new AlertDialog.Builder(this)
                .setTitle("Delete entry")
                .setMessage("Are you sure you want to delete this entry?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        doDelete(positionOfRecord);
                        returnToTowLogListViewActivity();
//
//                        Intent getTowLogIntent = new Intent(getApplicationContext(), TowLogListView.class);
//                        TowLogEdit.this.startActivity(getTowLogIntent);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Remove the tow log at the given position and reserialize data
      * @param positionOfRecord
     */
    private void doDelete(int positionOfRecord)
    {
        towLogs.remove(positionOfRecord);
        towLogData.serialize(towLogs, absolutePath);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tow_log_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    private void setUpGliderSpinner(int gliderID){


        //Add Collection of gliders after the default entry
        GliderData gliderData = TowPlaneApplication.getGliderData();
        ArrayList<Glider> gliders = gliderData.getGliders();


        gliderSpinner = (Spinner) findViewById(R.id.factor_XYZ);
        gliderSpinnerAdapter = new GliderSpinnerAdapter(this, R.layout.glider_custom_spinner_format, gliders);
        gliderSpinner.setAdapter(gliderSpinnerAdapter);

        SpinnerUtility.selectGliderSpinnerItemByID(gliderSpinner, gliderID);

        gliderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Glider glider = (Glider) adapterView.getItemAtPosition(i);
                gliderSpinnerAdapter.setSelectedItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getApplicationContext(), "There's nothing selected", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setUpPilotSpinner(int pilotID){

        //Add Collection of pilots after the default entry
        PilotData pilotData = TowPlaneApplication.getPilotData();
        ArrayList<Pilot> pilots = pilotData.getPilots();

        pilotSpinner = (Spinner) findViewById(R.id.speed_threshold);
        pilotSpinnerAdapter = new PilotSpinnerAdapter(this, R.layout.pilot_custom_spinner_format, pilots);
        pilotSpinner.setAdapter(pilotSpinnerAdapter);

        SpinnerUtility.selectPilotSpinnerItemByID(pilotSpinner, pilotID);

        pilotSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Pilot pilot = (Pilot) adapterView.getItemAtPosition(i);
                pilotSpinnerAdapter.setSelectedItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getApplicationContext(), "There's nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpTowPilotSpinner(int towPilotID) {
        //Add Collection of pilots after the default entry
        ArrayList<Pilot> towPilots = TowPlaneApplication.getPilotData().getTowPilots();

        towpilotSpinner = (Spinner) findViewById(R.id.towpilots);
        towpilotSpinnerAdapter = new PilotSpinnerAdapter(this, R.layout.pilot_custom_spinner_format, towPilots);
        towpilotSpinner.setAdapter(towpilotSpinnerAdapter);

        SpinnerUtility.selectPilotSpinnerItemByID(towpilotSpinner, towPilotID);

        towpilotSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Pilot pilot = (Pilot) adapterView.getItemAtPosition(i);
                towpilotSpinnerAdapter.setSelectedItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getApplicationContext(), "There's nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpNumberPickers(int height){

        nps[0] = (NumberPicker) findViewById(R.id.np10000);
        nps[0].setMaxValue(1);
        nps[0].setMinValue(0);
        nps[0].setOnValueChangedListener(onValueChanged);

        nps[1] = (NumberPicker) findViewById(R.id.np1000);
        nps[1].setMaxValue(9);
        nps[1].setMinValue(0);
        nps[1].setOnValueChangedListener(onValueChanged);

        nps[2] = (NumberPicker) findViewById(R.id.np100);
        nps[2].setMaxValue(9);
        nps[2].setMinValue(0);
        nps[2].setOnValueChangedListener(onValueChanged);

        nps[3] = (NumberPicker) findViewById(R.id.np10);
        nps[3].setMaxValue(9);
        nps[3].setMinValue(0);
        nps[3].setOnValueChangedListener(onValueChanged);

        //Limit this field to strictly 0
        nps[4] = (NumberPicker) findViewById(R.id.np1);
        nps[4].setValue(0);
        nps[4].setEnabled(false);
        nps[4].setOnValueChangedListener(onValueChanged);

        txtTowHeight = (TextView) findViewById(R.id.txtTowHeight);
        txtTowHeight.setText(String.valueOf(height));

        setNumberPickerToValue(height);
    }

    NumberPicker.OnValueChangeListener onValueChanged = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            int ones = nps[4].getValue();
            int tens = nps[3].getValue();
            int hundreds = nps[2].getValue();
            int thousands = nps[1].getValue();
            int tenthousands = nps[0].getValue();
            int total = tenthousands * 10000 + thousands *1000 + hundreds * 100 + tens * 10 + ones;
            txtTowHeight.setText(String.valueOf(total));
        }
    };

    public void setNumberPickerToValue(int value){
        String work = "00000" + Integer.toString(value);    //Pad the front of the string with zeros
        String foo = work.substring(work.length() - nps.length);
        for (int i = 0; i < nps.length; i++)
        {
            char character = foo.charAt(i);
            int valueToBeSet = Integer.parseInt("" + character);
            nps[i].setValue(valueToBeSet);
        }
    }
}
