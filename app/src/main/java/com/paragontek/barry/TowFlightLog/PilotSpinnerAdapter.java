package com.paragontek.barry.TowFlightLog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Brendan on 5/6/15.
 */
public class PilotSpinnerAdapter extends ArrayAdapter<Pilot> {

    private Context ctx;
    private ArrayList<Pilot> values;

    private int spinnerLayoutID;

    public int getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
    }

    private int selectedItem = 0;

    public PilotSpinnerAdapter(Context ctx, int spinnerLayoutID, ArrayList<Pilot> objects) {
        super(ctx, spinnerLayoutID, objects);
        this.ctx = ctx;
        this.values = objects;
        this.spinnerLayoutID = spinnerLayoutID;
    }

    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
//            Toast.makeText(getApplicationContext(), "Drop Down View selected", Toast.LENGTH_SHORT).show();
        return getCustomView(position, cnvtView, prnt);
    }

    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
//            Toast.makeText(getApplicationContext(), "Get View selected", Toast.LENGTH_SHORT).show();
        return getCustomView(pos, cnvtView, prnt);

    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View mySpinner = inflater.inflate(spinnerLayoutID, parent, false);

        String name = values.get(position).getName();
        String code = values.get(position).getCode();
//            String description = values.get(position).getDescription();

        TextView tvName = (TextView) mySpinner.findViewById(R.id.tvPilotName);
        tvName.setText(name);

        TextView tvCode = (TextView) mySpinner.findViewById(R.id.tvPilotCode);
        tvCode.setText(code);

//        ImageView left_icon = (ImageView) mySpinner.findViewById(R.id.left_pic);
////            left_icon.setImageResource(total_images[position]);
//        left_icon.setImageResource(R.drawable.abc_btn_check_to_on_mtrl_000);

        return mySpinner;
    }
}
