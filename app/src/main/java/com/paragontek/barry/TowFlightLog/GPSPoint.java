package com.paragontek.barry.TowFlightLog;

/**
 * Created by cbarry on 3/13/2016.
 */
public class GPSPoint {
    public double Latitude;
    public double Longitude;
    public double Height;

    public GPSPoint() {}

    public GPSPoint(double latitude, double longitude, double height) {
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.Height = height;
    }

    @Override
    public String toString() {
        return String.format("Point{Latitude %f Longitude %f Height %f", Latitude, Longitude, Height);
    }
}
