package com.paragontek.barry.TowFlightLog;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by cbarry on 3/13/2016.
 * A circular queue, where as we add items to the queue the oldest pops off
 * Adjacent readings are compared , and if they inside the threshold the readings are considered static
 */
public class BufferXYZ {
    ArrayList<XYZ> items = null;
    private int samples = 4;
    private double threshold = 16.0;

//    public BufferXYZ() {
//        items = new ArrayList<XYZ>(samples);
//    }

    public BufferXYZ(int samples, double threshold) {
        this.samples = samples;
        this.threshold = threshold;
        items = new ArrayList<XYZ>(samples);
    }

    /**
     * Add a number to the end of the circular queue
     *
     * @param item
     */
    public void enqueue(XYZ item) {
        if (items.size() == samples) {
            items.remove(0);
        }
        items.add(item);
    }

    public int size() {
        return items.size();
    }

    public boolean isFull() {
        return items.size() == samples;
    }

    public boolean isEmpty() {
        return items.size() == 0;
    }

    @Override
    public String toString() {
        return Arrays.toString(items.toArray());
    }

    public TFM getIsStable() {
        if (threshold == -1) {
            return TFM.eMaybe;
        }

        int count = 0;
        int prev = -1;

        for (int pos = 1; pos < items.size(); pos++) {
            prev = pos - 1;
            double delta = items.get(pos).getDelta(items.get(prev));
            if (delta * 10000000 > threshold)
                return TFM.eFalse;
            count++;
        }
        return TFM.eTrue;

//        return (count >= samples / 2) ? TFM.eTrue : TFM.eMaybe; // Over 50 % in range
    }

    public String getContents() {
        StringBuilder debug = new StringBuilder();
        debug.append(String.format("%d %.9f", items.size(), threshold));

        int count = 0;
        int prev = -1;
        for (int pos = 1; pos < items.size(); pos++) {
            prev = pos - 1;
            double delta = items.get(pos).getDelta(items.get(prev));
            String foo = String.format(" %.9f", delta * 10000000);
            debug.append(foo);
        }
        return debug.toString();
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }
}

//    public void TestCircularQueue() {
//         BufferXYZ q = new BufferXYZ(3);
//        Boolean result = true;
//        Double value = 0.0;
//        result = TestIt(q, false, 0);
//
//        q.enqueue(1.0);
//        result = TestIt(q, false, 1);
//        value = q.first();
//
//        q.enqueue(2.0);
//        result = TestIt(q, false, 2);
//        value = q.first();
//
//        q.enqueue(3.0);
//        result = TestIt(q, true, 3);
//        value = q.first();
//
//
//        q.enqueue(4.0);
//        result = TestIt(q, true, 3);
//        value = q.first();
//
//    }
//
//    private Boolean TestIt(BufferXYZ q, Boolean isFull, int size) {
//        Boolean testIsFull = isFull == q.isFull();
//        Boolean testSize = size == q.size();
//        return  testIsFull & testSize;
//
//    }
