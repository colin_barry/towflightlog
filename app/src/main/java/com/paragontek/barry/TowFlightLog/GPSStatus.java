package com.paragontek.barry.TowFlightLog;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class GPSStatus extends AppCompatActivity implements GPSInterface {
    public GPSTracker gps = null;
    boolean debugMode = false;

    private BufferXYZ bufferStabilityXYZ = null;
    private BufferDouble bufferAltitude = null;

    TowPlaneApplication app = null;
    TowPlaneState towPlaneState = null;


    ArrayAdapter<Integer> speedAdapter = null;
    ArrayAdapter<Double> altitudeAdapter = null;
    ArrayAdapter<Double> stabilityXYZAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        debugMode = settings.getBoolean("debug_mode_switch", false);

        app = ((TowPlaneApplication) this.getApplication());
        towPlaneState = app.getTowPlaneState();


        bufferStabilityXYZ = app.getBufferStabilityXYZ();
        bufferAltitude = app.getBufferAltitude();

        setContentView(R.layout.activity_gpsstatus);
        setUpGPSTracker();

        setUpSpeedThresholdSpinner();
        setUpAltitudeThreshholdSpinner();
        setUpFactorXYXSpinner();
    }

    private void setUpSpeedThresholdSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.speed_threshold);

        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int item = (int) adapterView.getItemAtPosition(i);
                app.setSpeedThresholdStartTow(item);

//                speedAdapter.setSelectedItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getApplicationContext(), "There's nothing selected", Toast.LENGTH_SHORT).show();
            }
        });

        // Spinner Drop down elements
        List<Integer> categories = new ArrayList<Integer>();
        for (int pos = 1; pos < 20; pos++) {
            categories.add(pos);
        }
        // Creating adapter for spinner
        speedAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        speedAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(speedAdapter);

        // select item
        int speedThresholdStartTow = app.getSpeedThresholdStartTow();
        int spinnerPosition = speedAdapter.getPosition(speedThresholdStartTow);
        spinner.setSelection(spinnerPosition);
    }

    private void setUpAltitudeThreshholdSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.AltitudeThreshold);

        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Double item = (Double) adapterView.getItemAtPosition(i);
                app.getBufferAltitude().setThreshold(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getApplicationContext(), "There's nothing selected", Toast.LENGTH_SHORT).show();
            }
        });

        // Spinner Drop down elements
        List<Double> categories = new ArrayList<Double>();
        for (int pos = 1; pos < 20; pos++) {
            double value = (double) pos / 10;
            categories.add(value);
        }
        // Creating adapter for spinner
        altitudeAdapter = new ArrayAdapter<Double>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        altitudeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(altitudeAdapter);

        // select item
        double altitudeThreshold = app.getBufferAltitude().getThreshold();
        int spinnerPosition = altitudeAdapter.getPosition(altitudeThreshold);
        spinner.setSelection(spinnerPosition);
    }

    private void setUpFactorXYXSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.factor_XYZ);

        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Double item = (Double) adapterView.getItemAtPosition(i);
                app.getBufferStabilityXYZ().setThreshold(item);
//                speedAdapter.setSelectedItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getApplicationContext(), "There's nothing selected", Toast.LENGTH_SHORT).show();
            }
        });

        // Spinner Drop down elements
        List<Double> categories = new ArrayList<Double>();
        for (int pos = 1; pos < 20; pos++) {
            double value = (double) pos;
            categories.add(value);
        }
        // Creating adapter for spinner
        stabilityXYZAdapter = new ArrayAdapter<Double>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        stabilityXYZAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(stabilityXYZAdapter);

        // select item
        double xyzThreshold = app.getBufferStabilityXYZ().getThreshold();
        int spinnerPosition = stabilityXYZAdapter.getPosition(xyzThreshold);
        spinner.setSelection(spinnerPosition);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (gps != null) {
            gps.unsetGPSTrackerListener(this);
        }
    }

    private void setUpGPSTracker() {
        gps = new GPSTracker(this);
        gps.setGPSTrackerListener(this);            // Register listener
    }

    /**
     * Implement interface for receiving altitude messages from GPSTracker
     *
     * @param message The message
     */
    @Override
    public void onGPSMessageReceived(String message) {
        //do something with the message, e.g. display to edit texts, lists etc.
        if (debugMode == true) {
            Toast t = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
            t.show();
        }

    }

    @Override
    public void onGPSSpeedReceived(double speedAsMetersPerSecond, double smoothedSpeedAsMetersPerSecond, Location location) {

        if (false) {
            String message = String.format("Speed Received: %.2f", smoothedSpeedAsMetersPerSecond);
            Toast t = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
            t.show();
        }

        TextView txtSpeed = (TextView) findViewById(R.id.txtSpeed);
        txtSpeed.setText(String.format("%.5f", smoothedSpeedAsMetersPerSecond));

        TextView lblSpeed = (TextView) findViewById(R.id.lblSpeed);

        changeBackColor(lblSpeed);

        processSpeed(smoothedSpeedAsMetersPerSecond);
        processStabilityXYZ(location);
        setTowPhase();
    }

    @Override
    public void onGPSSatelliteStatusEvent(int inView, int inFix) {

        if (false) {
            String message = String.format("Satellites InView:<%03d> InFix:<%03d>", inView, inFix);
            Toast t = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
            t.show();
        }

        TextView txtSatellitesInView = (TextView) findViewById(R.id.txtSatellitesInView);
        flashChanges(txtSatellitesInView, String.format("In view %03d", inView));

        TextView txtSatellitesInFix = (TextView) findViewById(R.id.txtSatellitesInFix);
        flashChanges(txtSatellitesInFix, String.format("In fix %03d", inFix));

        TextView lblSatellites = (TextView) findViewById(R.id.lblSatellites);
//
//        if (bufferStabilityXYZ != null) {
//            String foo = bufferStabilityXYZ.getContents();
//            TextView lblDeltaState = (TextView) findViewById(R.id.lblDeltaState);
//            lblDeltaState.setText(foo);
//        }

        changeBackColor(lblSatellites);
    }

    /**
     * The GPS has signaled a change in altitude
     * Update ui altitude
     *
     * @param altitudeAsMeters (in meters)
     */
    @Override
    public void onGPSAltitudeReceived(double altitudeAsMeters, double smoothedAltitudeAsMeters, Location location) {
        if (false) {
            String message = String.format("Altitude Received: %.2f", smoothedAltitudeAsMeters);
            Toast t = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
            t.show();
        }

        processAltitude(smoothedAltitudeAsMeters);

        TextView txtAltitude = (TextView) findViewById(R.id.txtAltitude);
        txtAltitude.setText(String.format("%.5f", smoothedAltitudeAsMeters));

        TextView lblAltitude = (TextView) findViewById(R.id.lblAltitude);
        changeBackColor(lblAltitude);

        TextView lblDeltaAltitude = (TextView) findViewById(R.id.lblDeltaAltitude);
        lblDeltaAltitude.setText(bufferAltitude.getContents());

        TextView lblAltitudeState = (TextView) findViewById(R.id.lblAltitudeState);
        String altitudeState = towPlaneState.getIsStableAltitudeText();
        flashChanges(lblAltitudeState, altitudeState);
    }

    private void processAltitude(double smoothedAltitudeAsMeters) {
        bufferAltitude.enqueue(smoothedAltitudeAsMeters);
        towPlaneState.setIsStableAltitude(bufferAltitude.getIsStable());
        setTowPhase();
    }

    private void processSpeed(double smoothedSpeedAsMetersPerSecond) {
        double walkingspeed = app.getSpeedThresholdStartTow();
        TFM isStableSpeed = smoothedSpeedAsMetersPerSecond <= walkingspeed ? TFM.eTrue : TFM.eFalse;
        towPlaneState.setIsStableSpeed(isStableSpeed);

        String stableSpeed = towPlaneState.getIsStableSpeedText();
        TextView lblSpeedState = (TextView) findViewById(R.id.lblSpeedState);
        flashChanges(lblSpeedState, stableSpeed);

        setTowPhase();

    }

    private void processStabilityXYZ(Location location) {
        bufferStabilityXYZ.enqueue(new XYZ(location));
        TFM isStableXYZ = bufferStabilityXYZ.getIsStable();
        towPlaneState.setIsStableSpeed(isStableXYZ);


        String stateOfStability = towPlaneState.getIsStableXYZText();
        TextView txtStationary = (TextView) findViewById(R.id.txtStability);
        flashChanges(txtStationary, stateOfStability);

        TextView lblDeltaStabilitylblDeltaStabilityXYZ = (TextView) findViewById(R.id.lblDeltaStabilityXYZ);
        lblDeltaStabilitylblDeltaStabilityXYZ.setText(bufferStabilityXYZ.getContents());

        setTowPhase();

    }


    private void flashChanges(TextView view, double value) {
        String currentValue = view.getText().toString();
        double valueAsDouble = Double.parseDouble(currentValue);
        if (value != valueAsDouble) {
            changeBackColor(view);
        }
    }

    private void flashChanges(TextView view, String value) {
        String currentValue = view.getText().toString();
        if (!value.equals(currentValue)) {
            view.setText(value);
            changeBackColor(view);
        }
    }

    private void changeBackColor(TextView view) {
        int tag = view.getTag() == null ? Color.YELLOW : (Integer) view.getTag();
        if (tag == Color.YELLOW) {
            view.setTextColor(Color.BLUE);
            view.setTag(Integer.valueOf(Color.BLUE));
        } else {
            view.setTextColor(Color.YELLOW);
            view.setTag(Integer.valueOf(Color.YELLOW));
        }
    }

    private void setTowPhase() {
        String towPhase = towPlaneState.getTowPhase();
        TextView txtTowPhase = (TextView) findViewById(R.id.txtTowPhase);
        txtTowPhase.setText(towPhase);
    }
}
