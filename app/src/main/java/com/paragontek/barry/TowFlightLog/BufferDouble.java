package com.paragontek.barry.TowFlightLog;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by cbarry on 3/19/2016.
 */
public class BufferDouble {
    ArrayList<Double> items = null;
    private int samples = 4;
    private double threshold = 0.5;

    public BufferDouble() {
        items = new ArrayList<Double>(samples);
    }

    public BufferDouble(int samples, double threshold) {
        this.samples = samples;
        this.threshold = threshold;
        items = new ArrayList<Double>(samples);
    }

    /**
     * Add a number to the end of the circular queue
     *
     * @param item
     */
    public void enqueue(Double item) {
        if (items.size() == samples) {
            items.remove(0);
        }
        items.add(item);
    }

    public int size() {
        return items.size();
    }

    public boolean isFull() {
        return items.size() == samples;
    }

    public boolean isEmpty() {
        return items.size() == 0;
    }

    @Override
    public String toString() {
        return Arrays.toString(items.toArray());
    }

    public TFM getIsStable() {
        if (threshold == -1) {
            return TFM.eMaybe;
        }

        int count = 0;
        int prev = -1;

        for (int pos = 1; pos < items.size(); pos++) {
            prev = pos - 1;
            double delta = (items.get(pos) * items.get(prev)) - (items.get(pos) * items.get(prev));
            if (delta > threshold * threshold)
                return TFM.eFalse;
            count++;
        }
        return  TFM.eTrue;

//        return (count >= samples / 2) ? TFM.eTrue : TFM.eMaybe; // Over 50 % in range
    }

    public String getContents() {
        StringBuilder debug = new StringBuilder();
        debug.append(String.format("%d %.9f", items.size(), threshold));

        int count = 0;
        int prev = -1;
        for (int pos = 1; pos < items.size(); pos++) {
            prev = pos - 1;
            double delta = (items.get(pos) * items.get(prev)) - (items.get(pos) * items.get(prev));
            String foo = String.format(" %.9f", delta);
            debug.append(foo);
        }
        return debug.toString();
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }
}

