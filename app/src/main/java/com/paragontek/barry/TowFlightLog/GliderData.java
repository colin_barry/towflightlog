package com.paragontek.barry.TowFlightLog;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 */
public class GliderData implements Serializable {
    private ArrayList<Glider> gliders = new ArrayList<Glider>();

    private static final String serializationName = "GliderData";

    public GliderData(){}

    public GliderData(ArrayList<Glider> gliders){
        this.gliders = gliders;
    }

    public ArrayList<Glider> getGliders() {
        return gliders;
    }

    public void setGliders(ArrayList<Glider> gliders) {
        this.gliders = gliders;
    }

    public boolean isEmpty(){
        return gliders == null || gliders.size() == 0;
    }

    protected Glider findGliderByCode(String code) {
        if (!this.gliders.isEmpty()) {
            for (int i = 0; i < this.gliders.size(); i++) {
                if (this.gliders.get(i).getCode().equals(code)) {
                    return gliders.get(i);
                }
            }
        }
        return null;
    }

    protected int findPositionByCode(String code){
        if (!this.gliders.isEmpty()) {
            for (int i = 0; i < this.gliders.size(); i++) {
                if (this.gliders.get(i).getCode().equals(code)) {
                    return i;
                }
            }
        }
        return -1;
    }

    protected Glider findInstanceByID(int id) {
        if (!this.gliders.isEmpty()) {
            for (Glider item : this.gliders) {
                if (item.getId() == id)
                    return item;
            }
        }
        return null;
    }


    protected void serialize(Context ctx) {
        File f = ctx.getFilesDir();
        final File suspend_f = new File(f.getAbsoluteFile() + File.separator + serializationName);

        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = new FileOutputStream(suspend_f);
            oos = new ObjectOutputStream(fos);

            oos.writeObject(gliders);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (oos != null) oos.close();
                if (fos != null) fos.close();
            } catch (Exception e) {
            }
        }
    }

    public void deserialize(Context ctx) {
        File f = ctx.getFilesDir();
        final File suspend_f = new File(f.getAbsoluteFile() + File.separator + serializationName);

        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(suspend_f);
            ois = new ObjectInputStream(fis);
            gliders = (ArrayList<Glider>) ois.readObject();
        } catch (Exception e) {
        } finally {
            try {
                if (ois != null) ois.close();
                if (fis != null) fis.close();
            } catch (Exception e) {
            }
        }
    }
}
