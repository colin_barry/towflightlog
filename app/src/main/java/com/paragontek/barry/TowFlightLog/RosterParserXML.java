package com.paragontek.barry.TowFlightLog;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 *
 * Unused Class, May be deleted
 * Handler to process XML Roster objects
 */
public class RosterParserXML {
    private List<Roster> rosters;
    private Roster roster;
    private String text;

    public RosterParserXML() {
        rosters = new ArrayList<Roster>();
    }

    public List<Roster> getRosters() {
        return rosters;
    }

    public List<Roster> parse(InputStream is) {
        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();

            parser.setInput(is, null);

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("BB_ROSTER")) {
                            roster = new Roster();
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("BB_ROSTER")) {
                            rosters.add(roster);
                        } else if (tagname.equalsIgnoreCase("CODE")) {
                            roster.setCode(text);
                        } else if (tagname.equalsIgnoreCase("FIRSTNAME")) {
                            roster.setFirstName(text);
                        } else if (tagname.equalsIgnoreCase("LASTNAME")) {
                            roster.setLastName(text);
                        } else if (tagname.equalsIgnoreCase("ISTOWPILOT")) {
                            roster.setIsTowPilot(false);
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return rosters;
    }

    private static XmlPullParserFactory newInstance() {
        return null;
    }

}
