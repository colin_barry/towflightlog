package com.paragontek.barry.TowFlightLog;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 *
 *
 * Handler to process JSON Tow Plane stream
 */
public class TowPlaneJSON extends JSONPull {

    private static final String URL = "http://ssb.michirado.com/flightlog/__towplanes.json";

    private static final String TAG_ID = "id";
    private static final String TAG_CODE = "name";
    private static final String TAG_DESCPRIPTION = "description";
    private static final String TAG_IS_ACTIVE = "is_active";
    private static final String TAG_IMAGE_URI = "image_url";


    private ArrayList<TowPlane> towPlanes = new ArrayList<TowPlane>();

    public TowPlaneJSON(Context ctx) {
        super(ctx, URL);
    }

    @Override
    protected void onPostExecute(JSONArray jArray) {
        super.onPostExecute(jArray);
        try {
            // Convert jArray to a collection
            if (jArray != null && jArray.length() > 0) {
                int siz = jArray.length();
                towPlanes = new ArrayList<TowPlane>(siz);
                for (int i = 0; i < siz; i++) {
                    JSONObject obj = jArray.getJSONObject(i);
                    TowPlane towPlane = new TowPlane();

                    int id = obj.getInt(TAG_ID);
                    String code = obj.getString(TAG_CODE);
                    String description = obj.getString(TAG_DESCPRIPTION);

                    String imageUri = null;
                    if (obj.has(TAG_IMAGE_URI)) {
                        imageUri = obj.getString(TAG_IMAGE_URI);
                    }
//                    Boolean getIsActive = obj.getBoolean(TAG_IS_ACTIVE);
                    Boolean isActive = true;        //Hardwired for now


                    towPlane.setId(id);
                    towPlane.setCode(code);
                    towPlane.setDescription(description);
                    towPlane.setIsActive(isActive);
                    towPlane.setImageUri(imageUri);

                    towPlanes.add(towPlane);            // Add to collection
                }

                TowPlaneData towPlaneData = new TowPlaneData();
                towPlaneData.setTowPlanes(towPlanes);
                Collections.sort(towPlanes);
                towPlaneData.serialize(ctx);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected ArrayList<TowPlane> getTowPlanes() {
        return towPlanes;
    }

    protected TowPlane findTowPlaneByCode(String code) {
        if (!this.towPlanes.isEmpty()) {
            for (int i = 0; i < this.towPlanes.size(); i++) {
                if (this.towPlanes.get(i).getCode().equals(code)) {
                    return towPlanes.get(i);
                }
            }
        }
        return null;
    }
}
