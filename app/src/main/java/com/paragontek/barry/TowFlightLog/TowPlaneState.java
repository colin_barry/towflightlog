package com.paragontek.barry.TowFlightLog;

import android.widget.TextView;

/**
 * Central point to define the tow plane state
 * Created by cbarry on 3/20/2016.
 */
public class TowPlaneState {

    private TFM isStableSpeed = TFM.eMaybe;
    private TFM isStableXYZ = TFM.eMaybe;
    private TFM isStableAltitude = TFM.eMaybe;

    private TFM lastStableSpeed = TFM.eMaybe;
    private TFM lastStableXYZ = TFM.eMaybe;
    private TFM lastStableAltitude = TFM.eMaybe;

    public String getTowPhase() {
        String towPhase = "Unknown";
        if (isStableAltitude == TFM.eTrue)
            towPhase = isStableSpeed == TFM.eFalse ? "Taxi" : "On ground";
        else if (isStableAltitude == TFM.eFalse )
            towPhase = "In flight";
        return towPhase;
    }

    public TFM getIsStableSpeed() {
        return isStableSpeed;
    }

    public String getIsStableSpeedText() {
        return isStableSpeed == TFM.eTrue ? "Slow" : "Moving";
    }

    public void setIsStableSpeed(TFM isStableSpeed) {
        lastStableSpeed = this.isStableSpeed;
        this.isStableSpeed = isStableSpeed;
    }

    public TFM getIsStableXYZ() {
        return isStableXYZ;
    }

    public String getIsStableXYZText() {
        return getStabilityText(isStableXYZ);
    }

    public void setIsStableXYZ(TFM isStableXYZ) {
        lastStableXYZ = this.isStableXYZ;
        this.isStableXYZ = isStableXYZ;
    }

    public TFM getIsStableAltitude() {
        return isStableAltitude;
    }

    public String getIsStableAltitudeText() {
        return getStabilityText(isStableAltitude);
    }

    public void setIsStableAltitude(TFM isStableAltitude) {
        lastStableXYZ = this.isStableAltitude;
        this.isStableAltitude = isStableAltitude;
    }

    private String getStabilityText(TFM value) {
        String stateOfStability = "Stable";
        if (value == TFM.eMaybe) {
            stateOfStability = "Undecided";
        } else if (value == TFM.eFalse) {
            stateOfStability = "In motion";
        }
        return stateOfStability;
    }
}
