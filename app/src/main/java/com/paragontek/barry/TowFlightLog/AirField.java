package com.paragontek.barry.TowFlightLog;

/**
 * Created by Brendan on 5/7/15.
 */
public class AirField {

    private int id;
    private String airPortCode;
    private int msl;

    public AirField(int id, String airPortCode, int msl) {
        this.id = id;
        this.airPortCode = airPortCode;
        this.msl = msl;
    }

    public String getAirPortCode() {
        return airPortCode;
    }

    public void setAirPortCode(String airPortCode) {
        this.airPortCode = airPortCode;
    }

    public int getMsl() {
        return msl;
    }

    public void setMsl(int msl) {
        this.msl = msl;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AirField{" +
                "id='" + id + '\'' +
                "airPortCode='" + airPortCode + '\'' +
                ", msl=" + msl +
                '}';
    }
}
