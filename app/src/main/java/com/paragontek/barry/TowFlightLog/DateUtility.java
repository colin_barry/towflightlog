package com.paragontek.barry.TowFlightLog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by cbarry on 9/13/2015.
 */
public class DateUtility {
    private static final String mDateTimeFormat = "yyyy-MMM-dd HH:mm:ss";

    public static String GetDateAsUTCString()
    {
        SimpleDateFormat f = new SimpleDateFormat(mDateTimeFormat);
        f.setTimeZone(TimeZone.getTimeZone("UTC"));
        return f.format(new Date());
    }

    public static String GetDateAsUTCString(long timeAsMillis) {
        SimpleDateFormat f = new SimpleDateFormat(mDateTimeFormat);
        f.setTimeZone(TimeZone.getTimeZone("UTC"));
        return f.format(new Date(timeAsMillis));
    }

    public static String GetDateAsLocal(long timeAsMillis) {
        SimpleDateFormat f = new SimpleDateFormat(mDateTimeFormat);
        TimeZone tz = TimeZone.getDefault();
        f.setTimeZone(tz);
        return f.format(new Date(timeAsMillis));
    }

    public static String GetTimeAsLocal(long timeAsMillis) {
        SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");
        TimeZone tz = TimeZone.getDefault();
        f.setTimeZone(tz);
        return f.format(new Date(timeAsMillis));
    }
}
