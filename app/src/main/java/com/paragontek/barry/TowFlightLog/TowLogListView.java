package com.paragontek.barry.TowFlightLog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 */
public class TowLogListView extends AppCompatActivity {

    ArrayList<TowLog> towLogs = null;
    String absolutePath = null;
    TowLogData towLogData = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow_log_list_view);

        TowPlaneApplication app = ((TowPlaneApplication) this.getApplication());
        towLogData = app.getTowLogData();

        // We may be coming from the tow log history activity
        // where the user has selected a tow log file
        // from the list of all the tow log 'days' serialized to storage
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            absolutePath = extras.getString("LogDataAbsolutePath");
        }
        else {
            absolutePath = towLogData.getFileNameForTheDay(TowLogListView.this);
        }

        if (absolutePath != null && absolutePath.length() > 0) {
            towLogs = towLogData.getTowLogsUsingAbsolutePath(TowLogListView.this, absolutePath);
        }
        else            // Find today's towlogs
        {
            towLogs = towLogData.getTowLogs();
        }

        Resources res = getResources();
        ListView list = (ListView) findViewById(R.id.list);

        TowLogListViewAdapter adapter = new TowLogListViewAdapter(this, towLogs, res);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(TowLogListView.this, TowLogEdit.class);
                intent.putExtra("Position", position);
                intent.putExtra("TowLogsCollection", towLogs);
                intent.putExtra("LogDataAbsolutePath", absolutePath);
                TowLogListView.this.startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_towlog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_main:
                Intent mainIntent = new Intent(this, MainActivity.class);
                TowLogListView.this.startActivity(mainIntent);
                break;
            case R.id.action_list_towlog_history:
                Intent towLogHistoryListView = new Intent(this, TowLogHistoryListView.class);
                TowLogListView.this.startActivity(towLogHistoryListView);
//            Toast.makeText(getApplicationContext(), "You clicked list tow log history.", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_save_towlog_to_xml:
//              Intent towLogXMLIntent = new Intent(this, TowLogXML.class);
//              TowLogListView.this.startActivity(towLogXMLIntent);
                Toast.makeText(getApplicationContext(), "You clicked save to towlog. This is work in progress", Toast.LENGTH_SHORT).show();


                break;
            case R.id.action_upload_to_server:
                doUpLoadToServer();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;

    }


    private void doUpLoadToServer()
    {
        Toast.makeText(getApplicationContext(), "Work in progress", Toast.LENGTH_SHORT).show();

    }



}
