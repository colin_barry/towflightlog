package com.paragontek.barry.TowFlightLog;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 */
public class GliderListView extends ActionBarActivity {

    public GliderListViewAdapter adapter = null;
    ListView list;
    ArrayList<Glider> gliders = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glider_listview);

        TowPlaneApplication app = ((TowPlaneApplication) this.getApplication());
        gliders = TowPlaneApplication.getGliderData().getGliders();

        Resources res = getResources();
        list = (ListView) findViewById(R.id.list);

        adapter = new GliderListViewAdapter(this, gliders, res);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Glider selectedGlider = gliders.get(position);
                String gliderCode = selectedGlider.getCode();
                String message = "You selected glider " + gliderCode;
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_return_to_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_main) {
            Intent mainIntent = new Intent(this, MainActivity.class);
            GliderListView.this.startActivity(mainIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
