package com.paragontek.barry.TowFlightLog;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 * <p/>
 * DataModel for Pilot row
 */
public class Pilot implements Serializable, Comparable<Pilot> {
    private int id;
    private String name;
    private String code;
    private String phone;
    private String imageUri;
    private Boolean isTowPilot;
    private int filterSequence;
    private Boolean isGliderPilot;
    private Boolean isInstructor;

    public String getImageUri() {
        return imageUri;
    }

    public Pilot() {
    }

    public Pilot(int id, String code, String name, String phone) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.phone = phone;
    }

//    public Pilot(Parcel source) {
//        this.id = source.readInt();
//        this.name = source.readString();
//        this.code = source.readString();
//        this.phone = source.readString();
//        this.imageUri = source.readString();
//        this.isTowPilot = source.readByte() != 0;     // myBoolean == true if byte != 0
//    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public Boolean getIsGliderPilot() {
        return isGliderPilot;
    }

    public void setIsGliderPilot(Boolean isGliderPilot) {
        this.isGliderPilot = isGliderPilot;
    }

    public Boolean getIsInstructor() {
        return isInstructor;
    }

    public void setIsInstructor(Boolean isInstructor) {
        this.isInstructor = isInstructor;
    }



//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel parcel, int i) {
//        parcel.writeInt(id);
//        parcel.writeString(name);
//        parcel.writeString(code);
//        parcel.writeString(phone);
//        parcel.writeString(imageUri);
//        parcel.writeByte((byte) (isTowPilot ? 1 : 0));
//    }

    @Override
    public String toString() {
        return "Pilot{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", phone='" + phone + '\'' +
                ", imageUri='" + imageUri + '\'' +
                '}';
    }

    public Boolean getIsTowPilot() {
        return isTowPilot;
    }

    public void setIsTowPilot(Boolean isTowPilot) {
        this.isTowPilot = isTowPilot;
    }

    /**
     * The filter sequence is used to change the default sort order of the collection
     * @return A sort order
     */
    public int getFilterSequence() {
        return filterSequence;
    }

    /**
     * The filter sequence is used to change the default sort order of the collection
     * @param filterSequence
     */
    public void setFilterSequence(int filterSequence) {
        this.filterSequence = filterSequence;
    }

    /**
     * Sorting order by code
     * Note: If filterSequence is set this is sorted before the pilot code
     * See: http://tutorials.jenkov.com/java-collections/sorting.html
     * @param other
     * @return
     */
    public int compareTo(Pilot other) {
        int result = filterSequence - other.getFilterSequence();
        if (result == 0)    // There the same filter sequence, so sort on code
        {
            return code.compareTo(other.code);
        }
        return result;
     }


}



