package com.paragontek.barry.TowFlightLog;

/**
 * Created by cbarry on 8/16/2015.
 * As described in: http://stackoverflow.com/questions/13323880/pass-data-from-class-to-activity
 */
public interface WifiInterface {
    void onWifiMessageReceived(String message);
//    public void onGPSAltitudeReceived(double altitude);
}
