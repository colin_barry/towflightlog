package com.paragontek.barry.TowFlightLog;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Brendan on 4/29/15.
 */
public class PilotListViewAdapter extends BaseAdapter {

    private ArrayList data;
    private static LayoutInflater inflater = null;
    Pilot pilotValue = null;
    private Activity activity;
    public Resources res;

    public PilotListViewAdapter(Activity a, ArrayList d, Resources resLocal){

        activity = a;
        data = d;
        res = resLocal;

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (data.size() <=0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public static class ViewHolder{

        public TextView text;
        public TextView name;
        public TextView phone;
        public ImageView image;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View vi = view;
        ViewHolder holder;

        if(vi == null){
            vi = inflater.inflate(R.layout.pilot_listview_format, null);

            holder = new ViewHolder();
            holder.text = (TextView) vi.findViewById(R.id.code);
            holder.name = (TextView) vi.findViewById(R.id.name);
            holder.phone= (TextView) vi.findViewById(R.id.phone);
            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        if(data.size()<=0) {
            holder.text.setText("No Data");
        }
        else
        {
            //Get each Model object from ArrayList
            pilotValue=null;
            pilotValue= ( Pilot ) data.get( i );

            //Set Model values in Holder elements
            holder.text.setText(pilotValue.getCode());
            holder.name.setText(pilotValue.getName());
            holder.phone.setText(pilotValue.getPhone());
        }
        return vi;
    }
}
