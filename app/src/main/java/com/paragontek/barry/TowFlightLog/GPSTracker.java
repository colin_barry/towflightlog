package com.paragontek.barry.TowFlightLog;

import android.content.Context;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.IBinder;
import android.provider.Settings;

import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by cbarry on 8/16/2015.
 * based on: http://www.androidhive.info/2012/07/android-gps-location-manager-tutorial/
 * and https://developer.android.com/training/location/receive-location-updates.html
 * and http://stackoverflow.com/questions/14222152/androids-onstatuschanged-not-working
 */
public class GPSTracker implements LocationListener,  GpsStatus.Listener {

    ArrayList<GPSInterface> listeners;
    private GpsStatus mGPSStatus;

    private final Context mContext;

    // flag for GPS status
    private boolean isGPSEnabled = false;

    // flag for network status
    private boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location

//    // The minimum distance to change Updates in meters
//    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
//
//    // The minimum time between updates in milliseconds
//    private static final long MIN_TIME_BW_UPDATES = 5000; // 5 seconds

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 2; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000; // 5 seconds

    // Declaring a Location Manager
    protected LocationManager locationManager;

    private int mSatellitesInView;
    private int mSatellitesUsedInFix;

    private LowPassFilter mSmoothedAltitude = null;
    private LowPassFilter mSmoothedSpeed = null;

    private static final double mLowPassFilterStartFactor = 0.3;
//    private static final double mMotionThresholdSpeed = 1.0;

    public GPSTracker(Context context) {
        this.mContext = context;
        listeners = new ArrayList<GPSInterface>();
        getLocation();
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            locationManager.addGpsStatusListener(this);
            // getting GPS status
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            // getting network status
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            isNetworkEnabled = false;

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

    /**
     * Accessor to get the latitude
     *
     * @return The latitude in decimal degrees if known, otherwise 0
     */
    public double getLatitude() {
        return (location != null) ? location.getLatitude() : 0;
    }

    /**
     * Accessor to get the longitude
     *
     * @return The longitude in decimal degrees if known, otherwise 0
     */
    public double getLongitude() {
        return (location != null) ? location.getLongitude() : 0;
    }

    /**
     * Accessor to get the altitude
     *
     * @return The altitude in meters if known, otherwise 0
     */
    public double getAltitude() {
        return (location != null) ? location.getAltitude() : 0;
    }

    /**
     * Get altitude using low pass filter
     * @return
     */
    public double getSmoothedAltitude() {
        return (location != null && mSmoothedAltitude != null) ? mSmoothedAltitude.getLastReading() : 0;
    }

    public int getSatellitesInView() {
        return (location != null) ? this.mSatellitesInView : 0;
    }

    public int getSatellitesUsedInFix() {
        return (location != null) ? this.mSatellitesUsedInFix : 0;
    }

    /**
     * Get speed
     * @return Speed
     */
    private double getSpeed()
    {
        return (location != null) ? location.getSpeed() : 0;
    }

    /**
     * Get speed via low pass filter
     * @return the low pass filter speed
     */
    public double getSmoothedSpeed() {
        return (location != null && mSmoothedSpeed != null) ? mSmoothedSpeed.getLastReading() : 0;
    }

    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;

        if (mSmoothedAltitude == null) {
            mSmoothedAltitude = new LowPassFilter(location.getAltitude(), mLowPassFilterStartFactor);
            broadcastGPSAltitudeEvent();
        }
        else 
        {
            Log.i("Altitude", String.format("%f %f", mSmoothedAltitude.getLastReading(), location.getAltitude()));
            mSmoothedAltitude.NewReading(location.getAltitude());
            broadcastGPSAltitudeEvent();
        }

//        if ( points == null) {
//            points = new BufferXYZ(10);
//        }
//
//        points.enqueue(new GPSPoint(location.getLatitude(), location.getLongitude(), mSmoothedAltitude.getLastReading()));

        if (mSmoothedSpeed == null) {
            mSmoothedSpeed = new LowPassFilter(location.getSpeed(), mLowPassFilterStartFactor);
            broadcastGPSSpeedEvent();
        }
        else
        {
            Log.i("Speed", String.format("%f %f", mSmoothedSpeed.getLastReading(), location.getSpeed()));
            mSmoothedSpeed.NewReading(location.getSpeed());
            broadcastGPSSpeedEvent();
        }

     }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    //        @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public String toString() {
        NumberFormat numberFormatter = NumberFormat.getNumberInstance();
        String latitude = numberFormatter.format(this.getLatitude());
        String longitude = numberFormatter.format(this.getLongitude());
        String altitude = numberFormatter.format(this.getAltitude());
        return String.format("Latitude:<%s> Longitude:<%s> Altitude:<%s>" , latitude, longitude, altitude);
    }

    @Override
    public void onGpsStatusChanged(int event) {
        switch (event) {
            case GpsStatus.GPS_EVENT_STARTED:
                //BroadcastGPSEvent("Do Something with mStatus info");
                break;

            case GpsStatus.GPS_EVENT_STOPPED:
                //BroadcastGPSEvent("Do Something with mStatus info");
                break;

            case GpsStatus.GPS_EVENT_FIRST_FIX:
                //BroadcastGPSEvent("Do Something with mStatus info");
                break;

            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                processSatelliteStatus();
                broadcastGPSSatelliteStatusEvent(getSatellitesInView(), getSatellitesUsedInFix());
                break;
        }
    }

    /**
     * Process the satellite information
     */
    private void processSatelliteStatus()
    {
        mSatellitesInView = 0;
        mSatellitesUsedInFix = 0;
        if (locationManager != null)
        {
            mGPSStatus = locationManager.getGpsStatus(null);
            if (mGPSStatus != null) {
                Iterable<android.location.GpsSatellite> satellites = mGPSStatus.getSatellites();
                if (satellites != null) {
                    for (android.location.GpsSatellite gpsSatellite : satellites) {
                        mSatellitesInView++;
                        if (gpsSatellite.usedInFix()) {
                            mSatellitesUsedInFix++;
                        }
                    }
                }
            }
        }
    }

    /**
     * Method to allow registration of listeners
     * @param listener
     */
    public void setGPSTrackerListener(GPSInterface listener){
        listeners.add(listener);
        Log.i("Listener added", String.format("Count %d", listeners.size()));
    }

    public void unsetGPSTrackerListener(GPSInterface listener){
        listeners.remove(listener);
        Log.i("Listener removed", String.format("Count %d", listeners.size()));
    }

    /**
     * Broadcast message to all registered listeners of a message received
     */
    private void broadcastGPSMessageEvent(String message)
    {
        for (GPSInterface listener:listeners) {
            listener.onGPSMessageReceived(message);
        }
    }

    private void broadcastGPSSatelliteStatusEvent(int inView, int inFix)
    {
        for (GPSInterface listener:listeners) {
            listener.onGPSSatelliteStatusEvent(inView, inFix);
        }
    }

    /**
     * Broadcast message to all registered listeners of an altitude change
    */
    private void broadcastGPSAltitudeEvent()
    {
        for (GPSInterface listener:listeners) {
            listener.onGPSAltitudeReceived(getAltitude(), getSmoothedAltitude(), this.location);
        }
    }

    /**
     * Broadcast message to all registered listeners of a speed change
     */
    private void broadcastGPSSpeedEvent()
    {
         for (GPSInterface listener:listeners) {
            listener.onGPSSpeedReceived(getSpeed(), getSmoothedSpeed(), this.location);
        }
    }
}
