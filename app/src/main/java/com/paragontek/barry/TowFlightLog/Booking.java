package com.paragontek.barry.TowFlightLog;

import java.io.Serializable;

/**
 * Created by cbarry on 12/10/2015.
 */
public class Booking implements Serializable {



    private int id;
    private int pilot1ID;
    private int pilot2ID;
    private int gliderID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPilot1ID() {
        return pilot1ID;
    }

    public void setPilot1ID(int pilot1ID) {
        this.pilot1ID = pilot1ID;
    }

    public int getPilot2ID() {
        return pilot2ID;
    }

    public void setPilot2ID(int pilot2ID) {
        this.pilot2ID = pilot2ID;
    }

    public int getGliderID() {
        return gliderID;
    }

    public void setGliderID(int gliderID) {
        this.gliderID = gliderID;
    }

    public String getCode() {
        return "foo";
    }

    @Override
    public String toString() {
        return "Booking{" +
                "pilot1ID=" + pilot1ID +
                ", pilot2ID=" + pilot2ID +
                ", gliderID=" + gliderID +
                '}';
    }
}
