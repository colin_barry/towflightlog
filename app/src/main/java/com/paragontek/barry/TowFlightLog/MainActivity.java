package com.paragontek.barry.TowFlightLog;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 */
public class MainActivity extends AppCompatActivity implements GPSInterface {

    TowPlaneApplication app = null;
    TowPlaneState towPlaneState = null;
    private BufferXYZ bufferStabilityXYZ = null;
    private BufferDouble bufferAltitude = null;


    GPSTracker gps = null;

    TowLog currentTowLog = null;

    Spinner gliderSpinner = null;
    Spinner pilotSpinner = null;
    GliderSpinnerAdapter gliderSpinnerAdapter = null;
    PilotSpinnerAdapter pilotSpinnerAdapter = null;
    AirField airField = null;

     // http://stackoverflow.com/questions/13022677/save-state-of-activity-when-orientation-changes-android

    Boolean towPlaneMoving = false;
//    long mTowStartMillis = 0;
//    long mTowEndMillis = 0;
//    long mTowMaxMillis = 0;
//    int mMaxAltitudeAsFeet = 0;
//    double mStartAltitudeAsMeters = 0;

    Boolean misPilotSpinnerFiltered = false;

    SharedPreferences settings = null;

//    TowLogData thisTowLog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize the application
        app = ((TowPlaneApplication) this.getApplication());
        currentTowLog = app.getCurrentTowLog();


        if (app == null || TowPlaneApplication.isEmpty()) {
            Toast.makeText(getApplicationContext(), "You need to load application data.", Toast.LENGTH_SHORT).show();
            Intent getDataIntent = new Intent(this, GetData.class);
            MainActivity.this.startActivity(getDataIntent);
            return;
        }

        towPlaneState = app.getTowPlaneState();
        bufferStabilityXYZ = app.getBufferStabilityXYZ();
        bufferAltitude = app.getBufferAltitude();

//        deserializeStandingData();

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = layoutInflater.inflate(R.layout.popup_main, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        AbsListView.LayoutParams.WRAP_CONTENT,
                        AbsListView.LayoutParams.WRAP_CONTENT);


                Button btnDismiss = (Button) popupView.findViewById(R.id.dismiss);
                btnDismiss.setOnClickListener(new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });

                Button btnDuplicate = (Button) popupView.findViewById(R.id.duplicate);
                btnDuplicate.setOnClickListener(new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ArrayList<TowLog> towLogs = TowPlaneApplication.getTowLogData().getTowLogs();
                        if (towLogs == null || towLogs.isEmpty() || towLogs.size() < 1) {
                            Toast.makeText(getApplicationContext(), "There are no records to duplicate", Toast.LENGTH_SHORT).show();
                            return;
                        }

//                        if (TowPlaneApplication.getTowLogData() == null || TowPlaneApplication.getTowLogData().isEmpty() || TowPlaneApplication.getTowLogData().getTowLogs().size() == 0){
//                            Toast.makeText(getApplicationContext(), "There are no records to duplicate", Toast.LENGTH_SHORT).show();
//                            return;
//                        }

                        TowLog thisTowLog = towLogs.get(towLogs.size() - 1);
                        String pilotCode = thisTowLog.getPilotCode();
                        int pilotID = thisTowLog.getPilotId();
                        String gliderCode = thisTowLog.getGliderCode();
                        String message = String.format("Record to duplicate: Pilot Code %s Glider Code %s", pilotCode, gliderCode);

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        //Set Pilot Spinner/Pilot ID to last record in tow log
                        SpinnerUtility.selectPilotSpinnerItemByID(pilotSpinner, pilotID);
                        //Set Glider Spinner/Glider ID to last record in tow log
                        SpinnerUtility.selectGliderSpinnerItemByCode(gliderSpinner, gliderCode);
                        popupWindow.dismiss();
                    }
                });

                Button btnFilter = (Button) popupView.findViewById(R.id.filter);
                btnFilter.setOnClickListener(new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "You Clicked Filter", Toast.LENGTH_SHORT).show();
                        // Demo of moving CJB to the top of the list
                        // 1. Save selected pilot
                        Pilot savedPilot = (Pilot) pilotSpinner.getSelectedItem();
                        int savedPilotID = savedPilot.getId();
                        // 2. Set filter sequence in list to bubble pilots with bookings to the top
                        PilotData pilotData = TowPlaneApplication.getPilotData();
                        pilotData.initializeFilterSequence();

                        BookingData bookingData = TowPlaneApplication.getBookingData();
                        if (!bookingData.isEmpty()) {
                            for (Booking item : bookingData.getBookings()) {
                                int pilot1ID = item.getPilot1ID();
                                Pilot pilot1 = pilotData.findInstanceByID(pilot1ID);
                                if (pilot1 != null) {
                                    pilot1.setFilterSequence(-99);
                                }
                                int pilot2ID = item.getPilot2ID();
                                if (pilot2ID > 0 && pilot2ID != pilot1ID) {
                                    Pilot pilot2 = pilotData.findInstanceByID(pilot2ID);
                                    if (pilot2 != null) {
                                        int sequence = pilot2.getIsInstructor() ? -77 : -99;
                                        pilot2.setFilterSequence(sequence);
                                    }
                                }
                            }
                        }

//                        Pilot foo1 = TowPlaneApplication.getPilotData().findInstanceByCode("CJB");
//                        foo1.setFilterSequence(-99);            // Change sequence in collection

                        setUpPilotSpinner(savedPilotID);      // Rebind spinner

                        popupWindow.dismiss();
                    }
                });

                popupWindow.showAsDropDown(fab, -200, -300);
            }
        });

        final Button btnOk = (Button) findViewById(R.id.btnUpdate);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTowLog();
            }
        });

        final Button btnPushPull = (Button) findViewById(R.id.btnPushPull);
        btnPushPull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPushPull();
            }
        });

        //We currently only handle one airport but have left the ability to have multiple airfields in the app
        AirFieldData airFieldData = new AirFieldData();
        airField = airFieldData.getAirFields().get(0);

        // Extract values from bundle, so we can reset activity
        int pilotID = currentTowLog.getGliderId();
        int gliderID = currentTowLog.getGliderId();

        setUpGliderSpinner(gliderID);
        setUpPilotSpinner(pilotID);

        setUpGPSTracker();

        settings = PreferenceManager.getDefaultSharedPreferences(this);

        //Will need to activate setAirfieldSelected once implemented properly.
        setAirfieldSelected();

        setTowPilotSelected();

//        displayMaxAltitudedValues();

//        sched = new ScheduledExample(this.getApplication());
//        sched.DoIt();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (gps != null) {
            gps.unsetGPSTrackerListener(this);
        }
    }

    private void setUpGPSTracker() {
        gps = new GPSTracker(this);
        gps.setGPSTrackerListener(this);            // Register listener
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        currentTowLog.setGliderId(-1);
        if (gliderSpinner != null) {
            Glider glider = (Glider) gliderSpinner.getSelectedItem();
            if (glider == null || glider.getId() != -1) {
                currentTowLog.setGliderId(glider.getId());
            }
        }

        currentTowLog.setPilotId(-1);
        if (pilotSpinner != null) {
            Pilot pilot = (Pilot) pilotSpinner.getSelectedItem();
             if (pilot == null || pilot.getId() != -1) {
                 currentTowLog.setPilotId(pilot.getId());
            }
        }

//        outState.putLong(TOW_START_MILLIS_VALUE, mTowStartMillis);
//        outState.putLong(TOW_END_MILLIS_VALUE, mTowEndMillis);
//        outState.putLong(TOW_MAX_MILLIS_VALUE, mTowMaxMillis);
//        outState.putInt(MAX_ALTITUDE_AS_FEET_VALUE, mMaxAltitudeAsFeet);
//        outState.putDouble(START_ALTITUDE_AS_METERS_VALUE, mStartAltitudeAsMeters);

        // Create a current tow log entry, and serialize it
//        currentTowLog = new TowLog();

    }

    private void setUpGliderSpinner(int gliderID) {

        Glider defaultEntry = new Glider();
        defaultEntry.setCode("Select glider");
        defaultEntry.setId(-1);
        ArrayList<Glider> gliders = new ArrayList<Glider>();
        gliders.add(defaultEntry);

        //Add Collection of gliders after the default entry
        GliderData gliderData = TowPlaneApplication.getGliderData();
        ArrayList<Glider> foo = gliderData.getGliders();
        gliders.addAll(foo);

        gliderSpinner = (Spinner) findViewById(R.id.factor_XYZ);
        gliderSpinnerAdapter = new GliderSpinnerAdapter(this, R.layout.glider_custom_spinner_format, gliders);
        gliderSpinner.setAdapter(gliderSpinnerAdapter);

        SpinnerUtility.selectGliderSpinnerItemByID(gliderSpinner, gliderID);

        gliderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Glider glider = (Glider) adapterView.getItemAtPosition(i);
                gliderSpinnerAdapter.setSelectedItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getApplicationContext(), "There's nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpPilotSpinner(int pilotID) {

        Pilot defaultEntry = new Pilot();
        defaultEntry.setCode("Select pilot");
        defaultEntry.setId(-1);
        ArrayList<Pilot> pilots = new ArrayList<Pilot>();
        pilots.add(defaultEntry);

        ArrayList<Pilot> foo = TowPlaneApplication.getPilotData().getPilots();
        // Sort into correct sequence.
        // if pilot.filterSequence is non zero this will take precedence over the pilot code
        Collections.sort(foo);

        //Add Collection of pilots after the default entry
        pilots.addAll(foo);

        pilotSpinner = (Spinner) findViewById(R.id.speed_threshold);
        pilotSpinnerAdapter = new PilotSpinnerAdapter(this, R.layout.pilot_custom_spinner_format, pilots);
        pilotSpinner.setAdapter(pilotSpinnerAdapter);

        SpinnerUtility.selectPilotSpinnerItemByID(pilotSpinner, pilotID);

    }

    /**
     * Setup for another tow
     */
    private void setUpForAnotherTow() {
        gliderSpinner.setSelection(0);
        pilotSpinner.setSelection(0);
        
        AirField airField = getAirFieldFromSettings();
        int airFieldMSL = airField.getMsl();
        double altitudeAsMeters = ConversionUtility.convertFeetToMeters(airFieldMSL);
        int maxAltitudeAsFeet = airFieldMSL - 1;
        setAltitudeValues(altitudeAsMeters, altitudeAsMeters);
        towPlaneMoving = false;
        long currentTimeAsMillis = System.currentTimeMillis();
//        mTowStartMillis = mTowEndMillis = mTowMaxMillis = currentTimeAsMillis;

        currentTowLog = new TowLog();
        currentTowLog.setPilotId(0);
        currentTowLog.setGliderId(0);
        currentTowLog.setTowStartMillis(currentTimeAsMillis);
        currentTowLog.setTowMaxMillis(currentTimeAsMillis);
        currentTowLog.setTowEndMillis(currentTimeAsMillis);
        currentTowLog.setAirportId(airField.getId());
        
        currentTowLog.setReleaseAltitude(maxAltitudeAsFeet);
        currentTowLog.setStartAltitude(maxAltitudeAsFeet);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case R.id.action_pilots:
                Intent pilotIntent = new Intent(this, PilotListView.class);
                MainActivity.this.startActivity(pilotIntent);
                break;
            case R.id.action_towplanes:
                Intent towPlaneIntent = new Intent(this, TowPlaneListView.class);
                MainActivity.this.startActivity(towPlaneIntent);
                break;
            case R.id.action_gliders:
                Intent gliderIntent = new Intent(this, GliderListView.class);
                MainActivity.this.startActivity(gliderIntent);
                break;
            case R.id.action_getdata:
                Intent getDataIntent = new Intent(this, GetData.class);
                MainActivity.this.startActivity(getDataIntent);
                break;
            case R.id.action_towlogs:
                Intent getTowLogIntent = new Intent(this, TowLogListView.class);
                MainActivity.this.startActivity(getTowLogIntent);
                break;
            case R.id.action_list_towlog_history:
                Intent towLogHistoryListView = new Intent(this, TowLogHistoryListView.class);
                this.startActivity(towLogHistoryListView);
//            Toast.makeText(getApplicationContext(), "You clicked list tow log history.", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_options:
                Intent getOptions = new Intent(this, Options.class);
                MainActivity.this.startActivity(getOptions);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    /**
     * Implement interface for receiving altitude messages from GPSTracker
     *
     * @param message The message
     */
    @Override
    public void onGPSMessageReceived(String message) {
        //do something with the message, e.g. display to edit texts, lists etc.
        boolean debugMode = settings.getBoolean("debug_mode_switch", false);

        if (debugMode == true) {
            Toast t = Toast.makeText(getApplicationContext(), "From Main " + message, Toast.LENGTH_SHORT);
            t.show();
        }
    }

    @Override
    public void onGPSSpeedReceived(double speedAsMetersPerSecond, double smoothedSpeedAsMetersPerSecond, Location location) {

        if (settings.getBoolean("debug_mode_switch", false)) {
            String message = String.format("Speed Received: %.2f", smoothedSpeedAsMetersPerSecond);
            Toast t = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
            t.show();
        }

        processSpeed(smoothedSpeedAsMetersPerSecond);
        processStabilityXYZ(location);
        setTowPhase();
    }

    /**
     * The GPS has signaled a change in altitude
     * Update ui altitude
     *
     * @param altitudeAsMeters (in meters)
     */
    @Override
    public void onGPSAltitudeReceived(double altitudeAsMeters, double smoothedAltitudeAsMeters, Location location) {
        processAltitude(smoothedAltitudeAsMeters);

        setAltitudeValues(altitudeAsMeters, smoothedAltitudeAsMeters);
     }

    @Override
    public void onGPSSatelliteStatusEvent(int inView, int inFix) {
        // Do something with the event
    }

    /**
     * Set text boxes to given value
     *
     * @param altitudeAsMeters
     */
    private void setAltitudeValues(double altitudeAsMeters, double smoothedAltitudeAsMeters) {
        TextView txtTowHeight = (TextView) findViewById(R.id.txtTowHeight);

        int smoothedAltitudeAFeet = ConversionUtility.convertMetersToFeet(smoothedAltitudeAsMeters);
        txtTowHeight.setText(String.valueOf(smoothedAltitudeAFeet));

        TextView txtCurrentTime = (TextView) findViewById(R.id.txtCurrentTime);
        txtCurrentTime.setText( DateUtility.GetTimeAsLocal(System.currentTimeMillis()));

        // Track maximum altitude
        if (currentTowLog.getReleaseAltitude() < smoothedAltitudeAFeet) {
            currentTowLog.setReleaseAltitude(smoothedAltitudeAFeet);
            currentTowLog.setTowMaxMillis(System.currentTimeMillis());
        }
        displayMaxAltitudeValues();
    }

    private void displayMaxAltitudeValues() {
        int releaseAltitude = currentTowLog.getReleaseAltitude();
        if (releaseAltitude >= 0) {
            TextView txtTowHeightMax = (TextView) findViewById(R.id.txtTowHeightMax);
            txtTowHeightMax.setText(String.valueOf(releaseAltitude));
        }

        long towMaxMillis = currentTowLog.getTowMaxMillis();
        if (towMaxMillis >= 0) {
            TextView txtTowMaxTimeValue = (TextView) findViewById(R.id.txtTowMaxTimeValue);
            txtTowMaxTimeValue.setText(DateUtility.GetTimeAsLocal(towMaxMillis));
        }
     }

    private void setAirfieldSelected(){
        TextView txtAirfield = (TextView) findViewById(R.id.txtAirfieldSelected);
        AirField airfield = getAirFieldFromSettings();
        String airportCode = (airfield == null) ? "TBA" : airfield.getAirPortCode();
        txtAirfield.setText(airportCode);
    }

    private void setTowPilotSelected(){
        TextView txtTowPilot = (TextView) findViewById(R.id.txtTowPilot);
        Pilot towPilot = getTowPilotFromSettings();
        String towPilotCode = (towPilot == null) ? "TBA" : towPilot.getCode();
        txtTowPilot.setText(towPilotCode);
    }


    private AirField getAirFieldFromSettings() {
        String airportCode = settings.getString("airport_list", "KBDU");
        if (airportCode.equals(null) || airportCode.isEmpty())
        {
            airportCode  = "KBDU";
        }
        AirFieldData airFieldData = new AirFieldData();
        AirField result = airFieldData.findInstanceByCode(airportCode);
        return result;
    }

    private Pilot getTowPilotFromSettings() {
        String towPilotCode = settings.getString("tow_pilot_list", "TBA");
        if (towPilotCode.equals(null) || towPilotCode.isEmpty())
        {
            towPilotCode  = "TBA";
        }
        PilotData pilotData = TowPlaneApplication.getPilotData();
        Pilot result = pilotData.findInstanceByCode(towPilotCode);
        return result;
    }

    private TowPlane getTowPlaneFromSettings() {
        String towPlaneCode = settings.getString("tow_plane_list", "TBA");
        if (towPlaneCode.equals(null) || towPlaneCode.isEmpty())
        {
            towPlaneCode  = "TBA";
        }
        TowPlaneData towPlaneData = TowPlaneApplication.getTowPlaneData();
        TowPlane result = towPlaneData.findInstanceByCode(towPlaneCode);
        return result;
    }


    /**
     * Extract the serialized data and load it into the application object
     */
    private void deserializeStandingData()
    {
        PilotData pilotData = new PilotData();
        pilotData.deserialize(MainActivity.this);
        app.setPilotData(pilotData);

        GliderData gliderData = new GliderData();
        gliderData.deserialize(MainActivity.this);
        app.setGliderData(gliderData);

        TowPlaneData towPlaneData = new TowPlaneData();
        towPlaneData.deserialize(MainActivity.this);
        app.setTowPlaneData(towPlaneData);

    }


    private void doPushPull() {
//        WifiTracker wifiTracker = null;
//
//        String message;
//        boolean hasInternetAccess = wifiTracker.hasInternetAccess();
//        if (hasInternetAccess) {
//            message = String.format("Wifi available at %s", wifiTracker.getSSID());
//        } else {
//            message = "Wifi not available";
//        }
        BookingJSON bookingJSON = new BookingJSON(MainActivity.this);
        bookingJSON.execute();

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        final Boolean isTestMode = settings.getBoolean("test_mode_switch", false);

        TowLogData towLogData = new TowLogData();
        towLogData.doUpLoadToServer(MainActivity.this, isTestMode);
    }

    public void saveTowLog() {

        TowLog towLog = new TowLog();
        
        AirField airField = getAirFieldFromSettings();
        if (airField == null) {
            Toast.makeText(getApplicationContext(), "Airfield is invalid", Toast.LENGTH_SHORT).show();
            return;
        }
        towLog.setAirportCode(airField.getAirPortCode());
        towLog.setAirportId(airField.getId());

        Pilot towPilot = getTowPilotFromSettings();
        if (towPilot == null) {
            Toast.makeText(getApplicationContext(), "Tow pilot is invalid", Toast.LENGTH_SHORT).show();
            return;
        }
        towLog.setTowPilotCode(towPilot.getCode());
        towLog.setTowPilotId(towPilot.getId());

        TowPlane towPlane = getTowPlaneFromSettings();
        if (towPlane == null) {
            Toast.makeText(getApplicationContext(), "Tow plane is invalid", Toast.LENGTH_SHORT).show();
            return;
        }
        towLog.setTowPlaneCode(towPlane.getCode());
        towLog.setTowPlaneId(towPlane.getId());

        Glider glider = (Glider) gliderSpinner.getSelectedItem();
        if (glider == null || glider.getId() == -1) {
            Toast.makeText(getApplicationContext(), "Glider code invalid.", Toast.LENGTH_SHORT).show();
            return;
        }
        String gliderCode = glider.getCode();
        towLog.setGliderCode(gliderCode);
        towLog.setGliderId(glider.getId());

        Pilot pilot = (Pilot) pilotSpinner.getSelectedItem();
        if (pilot == null || pilot.getId() == -1) {
            Toast.makeText(getApplicationContext(), "Pilot code invalid.", Toast.LENGTH_SHORT).show();
            return;
        }
        String pilotName = pilot.getName();
        String pilotCode = pilot.getCode();
        towLog.setPilotCode(pilotCode);
        towLog.setPilotId(pilot.getId());

//        if(mMaxAltitudeAsFeet < airField.getMsl())
//        {
//            Toast.makeText(getApplicationContext(), "Supplied tow height is less than airfield msl", Toast.LENGTH_SHORT).show();
//            return;
//        }
        /* Altitudes */
        towLog.setStartAltitude(currentTowLog.getStartAltitude());
        towLog.setReleaseAltitude(currentTowLog.getReleaseAltitude());
        towLog.setTowStartMillis(currentTowLog.getTowStartMillis());

        /* Times */
        towLog.setTowMaxMillis(currentTowLog.getTowMaxMillis());
        towLog.setTowStartMillis(currentTowLog.getTowStartMillis());
        towLog.setTowEndMillis(currentTowLog.getTowEndMillis());

        //Add the newly created entry to the tow log and serialize it just in case our system fails
        //We can recreate the records of tow logs from the serialized versions
        TowLogData towLogData = TowPlaneApplication.getTowLogData();

        towLogData.add(towLog);
        app.serializeTowLog(MainActivity.this);

        String message = String.format("Added Record: Pilot: %s in glider: %s to: %d", pilotName, gliderCode, currentTowLog.getReleaseAltitude());
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

        //Reset values for next entry to default
        setUpForAnotherTow();
    }

    private void processStabilityXYZ(Location location) {
        bufferStabilityXYZ.enqueue(new XYZ(location));
        TFM isStableXYZ =  bufferStabilityXYZ.getIsStable();
        towPlaneState.setIsStableSpeed(isStableXYZ);
        setTowPhase();
    }


    private void processSpeed(double smoothedSpeedAsMetersPerSecond) {
        double walkingspeed = app.getSpeedThresholdStartTow();
        TFM isStableSpeed = smoothedSpeedAsMetersPerSecond <= walkingspeed ? TFM.eTrue : TFM.eFalse;
        towPlaneState.setIsStableSpeed(isStableSpeed);
        setTowPhase();
    }

    private void processAltitude(double smoothedAltitudeAsMeters) {
        bufferAltitude.enqueue(smoothedAltitudeAsMeters);
        towPlaneState.setIsStableAltitude(bufferAltitude.getIsStable());
        setTowPhase();
    }

    private void setTowPhase() {
        String towPhase = towPlaneState.getTowPhase();
        TextView txtTowPhase = (TextView) findViewById(R.id.txtTowPhase);
        txtTowPhase.setText(towPhase);
    }
}
