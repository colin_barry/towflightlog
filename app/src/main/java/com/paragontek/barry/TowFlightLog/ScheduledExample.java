package com.paragontek.barry.TowFlightLog;

import android.content.Context;
import android.util.Log;

import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
/**
 * Created by cbarry on 9/12/2015.
 */
public class ScheduledExample {
    private Context mContext;
    private ScheduledThreadPoolExecutor sch = null;
    final static DateFormat fmt = DateFormat.getTimeInstance(DateFormat.LONG);



    public ScheduledExample(Context context){
        this.mContext = context;
        sch = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(5);
    }

    public void DoIt()
    {
        // Create a task for one-shot execution using schedule()
        Runnable oneShotTask = new Runnable(){
            @Override
            public void run() {
                Log.d("MyApp", "\t oneShotTask Execution Time: " + fmt.format(new Date()));
             }
        };

        // Create another task
        Runnable delayTask = new Runnable(){
            @Override
            public void run() {
                try{
                    for (int x= 0; x < 200; x++){
                        Log.d("MyApp", "\t delayTask Started: " + fmt.format(new Date()));
                    }

                     Thread.sleep(10 * 1000);
                    Log.d("MyApp", "\t delayTask Ended: " + fmt.format(new Date()));
                 }catch(Exception e){

                }
            }
        };

        // And yet another
        Runnable periodicTask = new Runnable(){
            @Override
            public void run() {
                try{
                    Log.d("MyApp", "\t periodicTask Started: " + fmt.format(new Date()));
                    Thread.sleep(10 * 1000);
                    Log.d("MyApp", "\t periodicTask Ended: " + fmt.format(new Date()));
                }catch(Exception e){

                }
            }
        };

        Log.d("MyApp", "Submission Time: " + fmt.format(new Date()));

//        ScheduledFuture<?> oneShotFuture = sch.schedule(oneShotTask, 5, TimeUnit.SECONDS);
        ScheduledFuture<?> delayFuture = sch.scheduleWithFixedDelay(delayTask, 5, 5, TimeUnit.SECONDS);
//        ScheduledFuture<?> periodicFuture = sch.scheduleAtFixedRate(periodicTask, 5, 5, TimeUnit.SECONDS);

    }
}
