package com.paragontek.barry.TowFlightLog;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.concurrent.ArrayBlockingQueue;

public class GetData extends ActionBarActivity implements AsyncTaskCompleted {

    //    BookingJSON bookingJSON;
    WifiTracker wifiTracker = null;

    TowPlaneApplication app = null;

    private Button btnRefresh = null;
    //    private Button btnSaveState = null;
    private Button btnLoadData = null;
    private Button btnCheckForWifi = null;
    private Button btnUploadData = null;
    private Button btnGetBookingData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_data);

        app = ((TowPlaneApplication) this.getApplication());
        app.deSerializeTowLogData(GetData.this);

        // https://developer.android.com/reference/android/app/IntentService.html Use Intent Service
        // Note the getting of the data, also serializes them
        btnRefresh = (Button) findViewById(R.id.btnRefresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doGetPilotData();
                doGetTowPlaneData();
                doGetGliderData();
                doGetBookingData();
            }
        });

        btnLoadData = (Button) findViewById(R.id.btnLoadData);
        btnLoadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deserializeStandingData();

                Toast.makeText(getApplicationContext(), "Data Loaded!!", Toast.LENGTH_SHORT).show();

            }
        });

        btnCheckForWifi = (Button) findViewById(R.id.btnCheckForWifi);
        btnCheckForWifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wifiTracker = new WifiTracker(GetData.this, GetData.this);
                wifiTracker.execute();              // Start thread
//                String message = wifiTracker.hasInternetAccess() ? "Wifi available" : "Wifi not available";
//                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });

        btnGetBookingData = (Button) findViewById(R.id.btnGetBookingData);
        btnGetBookingData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doGetBookingData();
            }
        });


        btnUploadData = (Button) findViewById(R.id.btnUploadData);
        btnUploadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doUpLoadToServer();
            }
        });

        Button btnGPSStatus = (Button) findViewById(R.id.btnGPSStatus);
        btnGPSStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GetData.this, GPSStatus.class);
                GetData.this.startActivity(intent);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_return_to_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_main) {
            Intent mainIntent = new Intent(this, MainActivity.class);
            GetData.this.startActivity(mainIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void asyncTaskCompleted() {
        String message;
        boolean hasInternetAccess = wifiTracker.hasInternetAccess();
        if (hasInternetAccess) {
            message = String.format("Wifi available at %s", wifiTracker.getSSID());
        } else {
            message = "Wifi not available";
        }
        enableButtons(hasInternetAccess);
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Enable/disable buttons, based on the ability to access WIFI
     *
     * @param hasInternetAccess
     */
    private void enableButtons(boolean hasInternetAccess) {
        btnRefresh.setEnabled(hasInternetAccess);
//        btnSaveState.setEnabled(hasInternetAccess);
        btnUploadData.setEnabled(hasInternetAccess);
        btnGetBookingData.setEnabled(hasInternetAccess);
    }

    private void doGetGliderData() {
        GliderJSON gliderJSON = new GliderJSON(GetData.this);
        gliderJSON.execute();
    }

    private void doGetBookingData() {
        BookingJSON bookingJSON = new BookingJSON(GetData.this);
        bookingJSON.execute();
    }

    private void doGetPilotData() {
        PilotJSON pilotJSON = new PilotJSON(GetData.this);
        pilotJSON.execute();
    }

    private void doGetTowPlaneData() {
        TowPlaneJSON towPlaneJSON = new TowPlaneJSON(GetData.this);
        towPlaneJSON.execute();
    }


    private void doUpLoadToServer() {
        app.deSerializeTowLogData(GetData.this);
        final TowLogData towLogData = app.getTowLogData();
        if (towLogData == null || towLogData.getTowLogs().isEmpty()) {
            Toast.makeText(getApplicationContext(), "No entries to upload to server", Toast.LENGTH_SHORT).show();
            return;
        }

        int entries = towLogData.getTowLogs().size();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        final Boolean isTestMode = settings.getBoolean("test_mode_switch", false);

        String message = String.format("%d entries ready for upload %s", entries, isTestMode ? " in test mode" : "");
        new AlertDialog.Builder(this)
                .setTitle("Tow Log Upload")
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        doUpLoadToServerAction(towLogData, isTestMode);
                        //
//                        Intent getTowLogIntent = new Intent(getApplicationContext(), TowLogListView.class);
//                        TowLogEdit.this.startActivity(getTowLogIntent);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void doUpLoadToServerAction(TowLogData towLogData, Boolean isTestMode) {
        towLogData.doUpLoadToServer(GetData.this, isTestMode);
//        if (towLogData != null) {
//            JSONArray results = towLogData.mapItemsToJSONArray(isTestMode);
//            String json = results.toString();
//            JSONPush upload = new JSONPush(GetData.this, towLogData.getUploadURL(), json);
//            upload.execute();
//        }
    }

    private void deserializeStandingData() {
        PilotData pilotData = new PilotData();
        pilotData.deserialize(GetData.this);
        app.setPilotData(pilotData);

        GliderData gliderData = new GliderData();
        gliderData.deserialize(GetData.this);
        app.setGliderData(gliderData);

        TowPlaneData towPlaneData = new TowPlaneData();
        towPlaneData.deserialize(GetData.this);
        app.setTowPlaneData(towPlaneData);

    }
}
