package com.paragontek.barry.TowFlightLog;

import android.location.Location;

/**
 * Created by cbarry on 3/15/2016.
 * Based on http://stackoverflow.com/questions/8981943/lat-long-to-x-y-z-position-in-js-not-working
 * We need to convert lat/lon to x/y/z vectors
 */
public class XYZ {
    public double X;
    public double Y;
    public double Z;

    XYZ(Location location) {
        setXYZ(location.getLatitude(), location.getLongitude());
    }

    private void setXYZ(double lat, double lon) {
        double cosLat = Math.cos(lat * Math.PI / 180.0);
        double sinLat = Math.sin(lat * Math.PI / 180.0);
        double cosLon = Math.cos(lon * Math.PI / 180.0);
        double sinLon = Math.sin(lon * Math.PI / 180.0);
        double rad = 500.0;
        X = rad * cosLat * cosLon;
        Y = rad * cosLat * sinLon;
        Z = rad * sinLat;
    }

    /**
     * Compare two objects and get the delta
     * @param other The other object
     * @return
     */
    public double getDelta(XYZ other)
    {
        double delta = (X - other.X) * (X - other.X) +
                (Y - other.Y) * (Y - other.Y) +
                (Z - other.Z) * (Z - other.Z);
        return delta;
    }

    @Override
    public String toString() {
        return String.format("XYZ X %f Y %f Z %f", X, Y, Z);
    }
}
