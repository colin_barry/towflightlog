package com.paragontek.barry.TowFlightLog;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * Created by Brendan Barry Student ID: S02099503 on 4/22/15.
 *
 * Handler to consume JSON Pilot feed
 */
public class PilotJSON extends JSONPull {

    private static final String URL = "http://ssb.michirado.com/flightlog/__pilots.json";

    private static final String TAG_ID = "id";
    private static final String TAG_CODE = "billing_initials";
    private static final String TAG_NAME = "name";
    private static final String TAG_PHONE = "cell_phone";
    private static final String TAG_IMAGE_URI = "image_url";
    private static final String TAG_IS_INSTRUCTOR = "is_instructor";
    private static final String TAG_IS_TOW_PILOT = "is_tow_pilot";
    private static final String TAG_IS_GLIDER_PILOT = "is_glider_pilot";

    private ArrayList<Pilot> pilots = new ArrayList<>();

    public PilotJSON(Context ctx) {
        super(ctx, URL);
    }

    @Override
    protected void onPostExecute(JSONArray jArray) {
        super.onPostExecute(jArray);
        try {
            // Convert jArray to a collection
            if (jArray != null && jArray.length() > 0) {
                int siz = jArray.length();
                pilots = new ArrayList<>(siz);
                for (int i = 0; i < siz; i++) {
                    JSONObject obj = jArray.getJSONObject(i);
                    Pilot pilot = new Pilot();

                    int id = obj.getInt(TAG_ID);
                    String name = obj.getString(TAG_NAME);
                    String code = obj.getString(TAG_CODE);
                    String phone = obj.getString(TAG_PHONE);

                    String imageUri = null;
                    if (obj.has(TAG_IMAGE_URI)) {
                        imageUri = obj.getString(TAG_IMAGE_URI);
                    }
                    int isTowPilot = obj.getInt(TAG_IS_TOW_PILOT);
                    int isInstructor = obj.getInt(TAG_IS_INSTRUCTOR);
                    int isGliderPilot = obj.getInt(TAG_IS_GLIDER_PILOT);

                    pilot.setCode(code);
                    pilot.setName(name);
                    pilot.setId(id);
                    pilot.setPhone(phone);
                    pilot.setImageUri(imageUri);
                    pilot.setIsTowPilot(isTowPilot == 1);
                    pilot.setIsInstructor(isInstructor == 1);
                    pilot.setIsGliderPilot(isGliderPilot == 1);

                    pilots.add(pilot);            // Add to collection
                }

                PilotData pilotData = new PilotData();
                pilotData.setPilots(pilots);
                Collections.sort(pilots);
                pilotData.serialize(ctx);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected ArrayList<Pilot> getPilots() {
        return pilots;
    }

    protected Pilot findPilotByCode(String code) {
        if (!this.pilots.isEmpty()) {
            for (int i = 0; i < this.pilots.size(); i++) {
                if (this.pilots.get(i).getCode().equals(code)) {
                    return pilots.get(i);
                }
            }
        }
        return null;
    }
}
